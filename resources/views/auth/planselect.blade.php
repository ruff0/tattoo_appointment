@extends('layouts.app')
@section('content')
  <div class="wrapper">
    <div class="main-panel">
      <div class="container">
        @include('inc.nav')
      </div>
      @include('inc.messages')
      <div class="content">
        <div class="container">
          <h2>Working</h2>
        </div>
      </div>
    </div>
  </div>
  <script src="https://js.stripe.com/v3/"></script>
@endsection
