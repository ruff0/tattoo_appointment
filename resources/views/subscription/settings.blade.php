@extends('layouts.app')
@section('content')
<div class="wrapper">
  @include('inc.sidebar')
  <div class="main-panel">
    @include('inc.nav')
    @include('inc.messages')
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="header">
                <h4 class="title">Subscription</h4>
                <p class="category">About your Subscription</p>
              </div>
              <div class="content">
                @if ($user->subscribed('main'))
                  @if ($user->subscription('main')->cancelled())
                    <h5>Subscription Status: Canceled</h5>
                    {{-- {{Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'))}}
                    <p>Your Subscription will end on {{Carbon\Carbon::parse($user->subscription('main')->asStripeSubscription()->current_period_end)->format('D d M Y')}}</p> --}}
                  @else
                    <h5>Subscription Status: Active</h5>
                  @endif
                  <ul>
                    <li>Subscription Type: {{$plan}}</li>
                    @if (!$user->subscription('main')->cancelled())
                      <li><a href="{{url('subscription/cancel')}}?_token={{ csrf_token() }}">Cancel Subscription</a></li>
                    @else
                      <li><a href="{{url('subscription/resume')}}?_token={{ csrf_token() }}">Resume Subscription</a></li>
                    @endif
                  </ul>
                @else
                  <p>You're not Subscribed</p>
                @endif
              </div>
              <div class="footer">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card">
              <div class="header">
                <h4 class="title">Card Details</h4>
                <p class="category">Edit Card Details</p>
              </div>
              <div class="content">
                @if ($user->subscribed('main'))
                  <ul>
                    <li>
                      <a href="{{url('subscription/card')}}?_token={{ csrf_token() }}">Update Card Info</a>
                    </li>
                  </ul>
                @endif
              </div>
              <div class="footer">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
