<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Customer;
use App\Appointment;
use Illuminate\Pagination\Paginator;
use Auth;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $loggedInUser = Auth::user();
      $user_id = $loggedInUser->id;
      // $customers = Customer::all()->paginate(15);
      $customers = \App\Customer::where("user_id", $user_id)->paginate(10);
      foreach ($customers as $customer) {
        $customer->dob = date_diff(date_create($customer->dob), date_create('today'))->y;
      }
      return view('customers/index', ['page'=> 'Customers', 'customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('customers/create', ['page'=> 'New Customer']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $loggedInUser = Auth::user();

      $this->validate($request, [
        'f_name' => 'required',
        'l_name' => 'required',
        'dob' => 'required',
        'address_line_1' => 'required',
        'city' => 'required',
        'country' => 'required',
        'email' => 'required',
        'phone_no' => 'required'
        // 'cover_image' => 'image|nullable|max1999'
      ]);

      // if($request->hasFile('file')){
      //   //Get Array of files
      //   $images = $request->file('file');
      //   $imageNamesToStore = [];
      //   //loop
      //   $i = 0;
      //   $size = count($images);
      //   foreach ($images as $image) {
      //     //Get Filename with Extension
      //     $filenameWithExt = $image->getClientOriginalName();
      //     //Get Just Filename
      //     $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
      //     //Just Extension
      //     $extension = $image->getClientOriginalExtension();
      //     //Filename to Store
      //     $fileNameToStore = $filename.'_'.time().'.'.$extension;
      //     //Add to Array
      //     $imageNamesToStore[] = $fileNameToStore;
      //     //Upload Image
      //     if ($i == $size-1) {
      //       $path = $image->storeAs('public/img/appointment_imgs/cover_image', $fileNameToStore);
      //     }
      //     else {
      //       $path = $image->storeAs('public/img/appointment_imgs', $fileNameToStore);
      //     }
      //     $i++;
      //   }
      //   // $fileNameToStore = 'working.jpg';
      // }
      // else {
      //   $fileNameToStore = 'defaultimage.jpg';
      //   $imageNamesToStore[] = 'defaultimage.jpg';
      // }

      $customer = new Customer;
      $customer->f_name = $request->input('f_name');
      $customer->l_name = $request->input('l_name');
      $customer->dob = \Carbon\Carbon::parse($request->input('dob'))->format('y/m/d');
      $customer->address_line_1 = $request->input('address_line_1');
      $customer->address_line_2 = $request->input('address_line_2');
      $customer->city = $request->input('city');
      $customer->country = $request->input('country');
      $customer->email = $request->input('email');
      $customer->phone_no = $request->input('phone_no');
      $customer->user_id = $loggedInUser->id;
      $customer->save();

      // $request->session()->flash('success', 'Appointment Created');

      // return redirect('customers')->with('page', 'Customers')->flash('success', 'Customer Created');
      return redirect('customers')->with(['page' => 'Customers', 'success' => 'Customer Created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $customer = Customer::where('id', $id)->get();
      $appointments = Appointment::where('client_id', $id)
        ->orderBy('appointment_date', 'desc')
        ->get();

      foreach ($customer as $cust) {
        $cust->age = date_diff(date_create($cust->dob), date_create('today'))->y;
      }

      // $appointments =
      // $dateOfBirth = date_diff(date_create($customer->dob), date_create('today'))->y;

      return view('customers.show', ['page'=> 'Customer View', 'customer' => $customer, 'appointments' => $appointments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      // $customer = Customer::where('id', $id)->get();
      $customer = Customer::find($id);
      $appointments = Appointment::where('client_id', $id)
        ->orderBy('appointment_date', 'desc')
        ->get();

      // foreach ($customer as $cust) {
      //   $cust->age = date_diff(date_create($cust->dob), date_create('today'))->y;
      // }
      $customer->age = date_diff(date_create($customer->dob), date_create('today'))->y;
      // $appointments =
      // $dateOfBirth = date_diff(date_create($customer->dob), date_create('today'))->y;

      return view('customers.edit', ['page'=> 'Edit View', 'customer' => $customer, 'appointments' => $appointments]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'f_name' => 'required',
        'l_name' => 'required',
        'dob' => 'required',
        'address_line_1' => 'required',
        'city' => 'required',
        'country' => 'required',
        'email' => 'required',
        'phone_no' => 'required'
        // 'cover_image' => 'image|nullable|max1999'
      ]);

      // if($request->hasFile('file')){
      //   //Get Array of files
      //   $images = $request->file('file');
      //   $imageNamesToStore = [];
      //   //loop
      //   $i = 0;
      //   $size = count($images);
      //   foreach ($images as $image) {
      //     //Get Filename with Extension
      //     $filenameWithExt = $image->getClientOriginalName();
      //     //Get Just Filename
      //     $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
      //     //Just Extension
      //     $extension = $image->getClientOriginalExtension();
      //     //Filename to Store
      //     $fileNameToStore = $filename.'_'.time().'.'.$extension;
      //     //Add to Array
      //     $imageNamesToStore[] = $fileNameToStore;
      //     //Upload Image
      //     if ($i == $size-1) {
      //       $path = $image->storeAs('public/img/appointment_imgs/cover_image', $fileNameToStore);
      //     }
      //     else {
      //       $path = $image->storeAs('public/img/appointment_imgs', $fileNameToStore);
      //     }
      //     $i++;
      //   }
      //   // $fileNameToStore = 'working.jpg';
      // }
      // else {
      //   $fileNameToStore = 'defaultimage.jpg';
      //   $imageNamesToStore[] = 'defaultimage.jpg';
      // }

      $customer = Customer::find($id);
      $customer->f_name = $request->input('f_name');
      $customer->l_name = $request->input('l_name');
      $customer->dob = \Carbon\Carbon::parse($request->input('dob'))->format('y/m/d');
      $customer->address_line_1 = $request->input('address_line_1');
      $customer->address_line_2 = $request->input('address_line_2');
      $customer->city = $request->input('city');
      $customer->country = $request->input('country');
      $customer->email = $request->input('email');
      $customer->phone_no = $request->input('phone_no');
      $customer->save();

      // $request->session()->flash('success', 'Appointment Created');

      // return redirect('customers')->with('page', 'Customers')->flash('success', 'Customer Updated');
      return redirect('customers')->with(['page' => 'Customers', 'success' => 'Customer Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $customer = Customer::find($id);
      $customer->delete();
      $appointments = Appointment::where('client_id', $id);
      foreach ($appointments as $appointment) {
        $appointment->delete();
      }
      return redirect('customers')->with(['page' => 'Customers', 'success' => 'Customer Deleted']);
    }
}
