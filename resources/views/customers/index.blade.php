@extends('layouts.app')
@section('content')
<div class="wrapper">
  @include('inc.sidebar')
  <div class="main-panel">
    @include('inc.nav')
    @include('inc.messages')
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                      <h3>Search Customers</h3>
                      <div class="row">
                        {{-- 'data-toggle'=>'validator' --}}
                        {!! Form::open(['action' => 'CustomerController@index', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                          <div class="col-sm-4">
                            <div class="form-group">
                              {{-- Title --}}
                              {{Form::label('name', 'Name')}}
                              {{Form::text('name', '', ['id'=>'nameToSearch', 'class'=>'form-control search-control', 'placeholder'=>'Name'])}}
                              {{-- <div class="help-block with-errors"></div> --}}
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              {{-- Client Info --}}
                              {{Form::label('email', 'Email')}}
                              {{Form::email('email', '', ['class'=>'form-control search-control', 'placeholder'=>'Email'])}}
                              {{-- <div class="help-block with-errors"></div> --}}
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <a href="customers/create" id="newCustomerBtn" class="pull-right btn btn-primary" type="button" name="button">New Customer</a>
                            </div>
                          </div>
                        {!! Form::close() !!}
                      </div>
                        {{-- <div class="form-group">
                          {{Form::label('file', 'Upload Multiple Reference Meterial')}}
                        </div> --}}
                        {{-- <p class="category">Here is a subtitle for this table</p> --}}
                      </div>
                      <div id="customers">
                        @include('customers')
                      </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var delay = (function(){
    var timer = 0;
    return function(callback, ms){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
    };
  })();
  $(document).on('click', '.pagination a',function(e){
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    getCustomers(page);
  });
  function getCustomers(page) {
    console.log('getting customers for page =' + page);
    $.ajax({
      url: 'ajax/customers?page='+page
    }).done(function(data){
      console.log(data);
      $('#customers').html(data);
    });
  }
  $(document).on('keyup', 'input.search-control',function(e){
    // e.preventDefault();
    var name = "";
    var email = "";
    $('input.search-control').each(function(index, value){
      // console.log($(this).val());
      if (index == 0) {
        name = $(this).val();
      }
      if (index == 1) {
        email = $(this).val();
      }
      // console.log("Name" + ": " + name + "\nEmail" + ": " + email);
    });
    delay(function(){
      if ((name == "") && (email == "")) {
        getCustomers(1);
      }
      else {
        getCustomersSearch(name, email);
      }
    }, 500 );
    // getCustomersSearch();
  });
  function getCustomersSearch(name, email) {
    var searchURL = "ajax/customersearch?";
    if($.trim(name).length){
      searchURL = searchURL + "name=" + name;
    }
    if(($.trim(email).length) && (!$.trim(name).length)){
      searchURL = searchURL + "email=" + email;
    }
    if(($.trim(email).length) && ($.trim(name).length)){
      searchURL = "ajax/customersearch?name=" + name + "&email=" + email;
    }
    $.ajax({
      url: searchURL
    }).done(function(data){
      $('#customers').html(data);
    });
  }
</script>
@endsection
