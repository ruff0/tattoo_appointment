<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SubscribedMiddleware
{
    protected $except_urls = [
        'settings',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
       $loggedInUser = Auth::user();
       $user_id = $loggedInUser->id;
       if ($loggedInUser->subscribed('main')) {
         if ($loggedInUser->subscription('main')->cancelled()){
           // This user is not a paying customer...
           return redirect('settings');
         }
       }

       return $next($request);
     }
}
