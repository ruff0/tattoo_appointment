@extends('layouts.app')
  @section('content')
  <div class="wrapper">
    @include('inc.backsidebar')
    <div class="main-panel">
      @include('inc.nav')
      @include('inc.messages')
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"
              id="favoritesModalLabel">Edit Appointment</h4>
            </div>
            <div class="modal-body">
              {!! Form::open(['action' => ['AppointmentController@update', $appointment->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class'=>'create-form', 'data-toggle'=>'validator']) !!}
              <div class="form-group">
                {{Form::label('clientid', 'Customer Name')}}
                {{Form::text('clientid', $appointment->customer->f_name . " " .  $appointment->customer->l_name, ['class'=>'form-control', 'placeholder'=>'title', 'disabled'])}}
              </div>
              <div class="form-group">
                {{-- Title --}}
                {{Form::label('title', 'Title')}}
                {{Form::text('title', $appointment->title, ['class'=>'form-control', 'placeholder'=>'title', 'required'])}}
                <div class="help-block with-errors"></div>
              </div>

              <div class="form-group">
                {{Form::label('date', 'Date')}}
                {{Form::text('date', $appointment->appointment_date, ['id'=>'datePicker'])}}
            <div class="row">
              <div class="col-sm-6 pull-left">
                {{Form::label('starttime', 'Start Time')}}<br>
                {{Form::text('starttime', $appointment->start_time, ['id'=>'startTimePicker'])}}
              </div>
              <div class="col-sm-6 pull-right">
                {{Form::label('endtime', 'Duration in Hours')}}<br>
                {{Form::text('endtime', $appointment->end_time, ['id'=>'endTimePicker'])}}
              </div>
            </div>
          </div>
          <script type="text/javascript">
          $(function () {
            $('#datePicker').datetimepicker({
              inline: true,
              sideBySide: true,
              format : 'YYYY/MM/DD'
            });
            $('#startTimePicker').datetimepicker({
              inline: true,
              sideBySide: true,
              format : 'HH:mm',
              // enabledHours: [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22]
            });
            $('#endTimePicker').datetimepicker({
              inline: true,
              sideBySide: true,
              format : 'HH:mm'
            });
          });
        </script>

        <div class="form-group">
          {{-- Description --}}
          {{Form::label('description', 'Description')}}
          {{Form::textArea('description', $appointment->description, ['class'=>'form-control', 'placeholder'=>'description', 'required'])}}
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
          {{Form::label('totalprice', 'Total Price')}}
          {{Form::number('totalprice', $appointment->total_cost, ['class'=>'form-control', 'placeholder'=>'200', 'value'=>'null', 'required'])}}
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
          {{Form::label('deposit', 'Deposit Paid')}}
          {{Form::number('deposit', $appointment->deposit_paid, ['class'=>'form-control', 'placeholder'=>'50', 'value'=>'null', 'required'])}}
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
          {{Form::label('amounttopay', 'Amount to Pay')}}
          {{Form::number('amounttopay', $appointment->amount_to_pay, ['class'=>'form-control', 'placeholder'=>'150', 'readonly'])}}
        </div>
        <div class="form-group">
          {{-- References --}}
          {{-- {{Form::label('file', 'Upload More Reference Meterial')}} --}}
        </div>
        <div class="form-group">
          {{-- {{$lengthofimages}} --}}
          {{-- @if ( $appointment->cover_image != "defaultimage.jpg")
            @foreach ($images as $image)
              @if ( $image != end($images))
                <div class="slick-slider-nav-img" style="background-image:url('{{ URL::to('/') }}/storage/img/appointment_imgs/{{$image}}');"></div>
              @else
                <div class="slick-slider-nav-img" style="background-image:url('{{ URL::to('/') }}/storage/img/appointment_imgs/cover_image/{{$image}}');"></div>
              @endif
            @endforeach
          @else
            <p class="text-center">No Reference Images Saved Previously</p>
          @endif --}}
        </div>

      </div>
      <div class="modal-footer">
        <span class="pull-right">
          {{Form::hidden('_method', 'PUT')}}
          {{Form::submit('Update', ['class'=>'btn btn-primary', 'id'=>'sbmtbtn'])}}
        </span>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection
