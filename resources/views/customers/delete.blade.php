{{-- {{$customer}} --}}
@extends('layouts.app')
  @section('content')
  <div class="wrapper">
    @include('inc.backsidebar')
    <div class="main-panel">
      @include('inc.nav')
      @include('inc.messages')
      <h2>Are you sure you want to delete {{$customer->f_name}}</h2>
      <div class="content">
        <div class="card">
          {!! Form::open(['action' => ['CustomerController@update', $customer->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class'=>'create-form', 'data-toggle'=>'validator']) !!}
            {{Form::hidden('_method', 'PUT')}}
          {{Form::submit('Update', ['class'=>'btn btn-danger pull-right', 'id'=>'sbmtbtn'])}}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endsection
