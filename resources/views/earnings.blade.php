{{-- @foreach ($appointments as $appointment)
  <a href="appointments/{{$appointment->id}}">
    <div class="panel panel-default slideInUp">
      <div class="panel-body">
        <h5 class="pull-left">{{$appointment->title}} for {{$appointment->client_name}}</h5>
        <span class="pull-right">{{$appointment->appointment_date}} @ {{substr($appointment->start_time, 0, -3)}}</span>
      </div>
    </div>
  </a>
@endforeach
{{ $appointments->links() }} --}}


@if (count($appointments) == 0)
<div class="content table-responsive table-full-width">
  <h2 class="text-center">No Results.</h2>
</div>
@else
<div class="content table-responsive table-full-width">
    <table id="earnings-table" class="table table-hover table-striped">
        <thead>
          <th>€ Earnings</th>
          <th>Status</th>
          <th>Title</th>
          <th>Date</th>
          <th>For (Name)</th>
        </thead>
        <tbody>
          @foreach ($appointments as $appointment)
            <tr>
              @if ($appointment->complete)
                <td>€{{$appointment->total_cost}}</td>
                <td>Complete</td>
              @elseif ($appointment->canceled)
                <td>€{{$appointment->deposit_paid}}</td>
                <td>Cancled</td>
              @elseif ($appointment->refunded)
                <td>€0</td>
                <td>Refunded</td>
              @else
                <td>€{{$appointment->total_cost}}</td>
                <td>Status Not Marked</td>
              @endif
              <td>{{$appointment->title}}</td>
              <td>{{$appointment->appointment_date}}</td>
              <td>{{$appointment->customer->f_name}} {{$appointment->customer->l_name}}</td>
            </tr>
          @endforeach
        </tbody>
    </table>
    <div class="footer text-center">
      {{ $appointments->links() }}
    </div>
</div>
<script>
    $(".delete").on("submit", function(){
        return confirm("Do you want to delete this customer and all related appointments?");
    });
</script>
@endif
