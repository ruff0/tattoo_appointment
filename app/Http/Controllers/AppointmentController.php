<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use Illuminate\Support\Facades\Auth;
use App\User;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $loggedInUser = Auth::user();
      $user_id = $loggedInUser->id;

      $appointments = Appointment::where('user_id', $user_id)->get();

      $events = [];


      foreach ($appointments as $appointment) {
        //Get owner Name
        $owner = User::findOrFail($appointment->user_id)->name;

        //Get Start Date
        $date = Carbon::parse($appointment->appointment_date);
        $date = trim(substr($date, 0, -9));
        $startTime = Carbon::parse($appointment->start_time);
        $startTime = trim(substr($startTime, -8));
        $endTime = Carbon::parse($appointment->end_time);
        $endTime = trim(substr($endTime, -8));

        $startDateAndTime = date('Y-m-d H:i:s', strtotime("$date $startTime"));
        $endDateAndTime = date('Y-m-d H:i:s', strtotime("$date $endTime"));
        $startDateAndTime = str_replace(" ", "T", $startDateAndTime);
        $endDateAndTime = str_replace(" ", "T", $endDateAndTime);

        $appointmentID = $appointment->id;
        $title = $appointment->title;

        $color;
        if ($appointment->complete == 1) {
          $color = '#87CB16';
        }
        elseif ($appointment->canceled == 1) {
          $color = '#ED8D00';
        }
        elseif ($appointment->refunded == 1) {
          $color = '#EE2D20';
        }
        else {
          $color = '#2079ee';
        }

        $events[] = \Calendar::event(
          $title,
          false, //full day event?
          $startDateAndTime,
          $endDateAndTime,
          'stringEventId', //optionally, you can specify an event ID
          [
            'url'=>'/appointments/' . $appointmentID,
            'color'=>$color,
            'start'=>$startDateAndTime,
            'end'=>$endDateAndTime
          ]
        );
      }

      $calendar = \Calendar::addEvents($events)->setOptions([
        'firstDay' => 1,
        'minTime' =>'09:00:00',
        'maxTime' =>'23:00:00',
        'height' => '700'])->setCallbacks([]);

      return view('appointments.index', array('user'=>$loggedInUser, 'calendar'=>$calendar, 'page' => "Appointments"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //   // return redirect('/test');
    //   $this->validate($request, [
    //     'title' => 'required',
    //     'description' => 'required',
    //     'clientname' => 'required',
    //     'clientphone' => 'required',
    //     'totalprice' => 'required',
    //     'cover_image' => 'image|nullable|max1999'
    //   ]);
    //
    //   if($request->hasFile('file')){
    //     //Get Array of files
    //     $images = $request->file('file');
    //     $imageNamesToStore = [];
    //     //loop
    //     $i = 0;
    //     $size = count($images);
    //     foreach ($images as $image) {
    //       //Get Filename with Extension
    //       $filenameWithExt = $image->getClientOriginalName();
    //       //Get Just Filename
    //       $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
    //       //Just Extension
    //       $extension = $image->getClientOriginalExtension();
    //       //Filename to Store
    //       $fileNameToStore = $filename.'_'.time().'.'.$extension;
    //       //Add to Array
    //       $imageNamesToStore[] = $fileNameToStore;
    //       //Upload Image
    //       if ($i == $size-1) {
    //         $path = $image->storeAs('public/img/appointment_imgs/cover_image', $fileNameToStore);
    //       }
    //       else {
    //         $path = $image->storeAs('public/img/appointment_imgs', $fileNameToStore);
    //       }
    //       $i++;
    //     }
    //     // $fileNameToStore = 'working.jpg';
    //   }
    //   else {
    //     $fileNameToStore = 'defaultimage.jpg';
    //     $imageNamesToStore[] = 'defaultimage.jpg';
    //   }
    //
    //   $appointment = new Appointment;
    //   $appointment->user_id = auth()->user()->id;
    //   $appointment->title = $request->input('title');
    //   $appointment->description = $request->input('description');
    //   $appointment->client_name = $request->input('clientname');
    //   $appointment->client_phone = $request->input('clientphone');
    //   $appointment->total_cost = $request->input('totalprice');
    //   $appointment->deposit_paid = $request->input('deposit');
    //   $appointment->amount_to_pay = $request->input('amounttopay');
    //   $appointment->appointment_date = \Carbon\Carbon::parse($request->input('date'))->format('y/m/d');
    //   $appointment->start_time = $request->input('starttime');
    //   $appointment->end_time = $request->input('endtime');
    //   $appointment->cover_image = $fileNameToStore;
    //   $appointment->reference_json = json_encode($imageNamesToStore);
    //   $appointment->save();
    //
    //   $request->session()->flash('success', 'Appointment Created');
    //
    //   // return redirect('/')->with('success', 'Appointment Created');
    //   // return 'success';
    //   return $request->session()->flash('success', 'Appointment Created');
    // }
    public function store(Request $request)
    {
      // return redirect('/test');
      $this->validate($request, [
        'clientid' => 'required',
        'title' => 'required',
        'description' => 'required',
        // 'clientname' => 'required',
        // 'clientphone' => 'required',
        'totalprice' => 'required',
        'cover_image' => 'image|nullable|max1999'
      ]);

      if($request->hasFile('file')){
        //Get Array of files
        $images = $request->file('file');
        $imageNamesToStore = [];
        //loop
        $i = 0;
        $size = count($images);
        foreach ($images as $image) {
          //Get Filename with Extension
          $filenameWithExt = $image->getClientOriginalName();
          //Get Just Filename
          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
          //Just Extension
          $extension = $image->getClientOriginalExtension();
          //Filename to Store
          $fileNameToStore = $filename.'_'.time().'.'.$extension;
          //Add to Array
          $imageNamesToStore[] = $fileNameToStore;
          //Upload Image
          if ($i == $size-1) {
            $path = $image->storeAs('public/img/appointment_imgs/cover_image', $fileNameToStore);
          }
          else {
            $path = $image->storeAs('public/img/appointment_imgs', $fileNameToStore);
          }
          $i++;
        }
        // $fileNameToStore = 'working.jpg';
      }
      else {
        $path = $image->storeAs('public/img/noimage.jpg');
      }

      $appointment = new Appointment;
      $appointment->user_id = auth()->user()->id;
      $appointment->client_id = $request->input('clientid');
      $appointment->title = $request->input('title');
      $appointment->description = $request->input('description');
      // $appointment->client_name = $request->input('clientname');
      // $appointment->client_phone = $request->input('clientphone');
      $appointment->total_cost = $request->input('totalprice');
      $appointment->deposit_paid = $request->input('deposit');
      $appointment->amount_to_pay = $request->input('amounttopay');
      $appointment->appointment_date = \Carbon\Carbon::parse($request->input('date'))->format('y/m/d');
      $appointment->start_time = $request->input('starttime');
      $appointment->end_time = $request->input('endtime');
      $appointment->cover_image = $fileNameToStore;
      $appointment->reference_json = json_encode($imageNamesToStore);
      $appointment->complete = 0;
      $appointment->canceled = 0;
      $appointment->refunded = 0;

        $count = 0;
        $bcount = 0;
        $ccount = 0;

        $st = $appointment->start_time;
        $et = $appointment->end_time;
        $d = $appointment->appointment_date;
        $count = Appointment::whereBetween('end_time',[$st,$et])->where('appointment_date',$d)->where('canceled',0)->count();
        $bcount = Appointment::whereBetween('start_time',[$st,$et])->where('appointment_date',$d)->where('canceled',0)->count();
        $ccount = Appointment::where('end_time','>=',$appointment->end_time)
                            ->where('start_time','<=',$appointment->start_time)->where('appointment_date',$d)->where('canceled',0)
                            ->count();

        $count = $count + $bcount + $ccount;
        if ($count > 0)
        {
                  // return view('appointments.show', array('page' => 'Appointment', 'appointment'=>$appointment, 'artistName'=>$artistName, 'images'=>json_decode($images, true)));
        $request->session()->flash('error', 'Appointment Failed to Create, check your dates.');
        return $request->session()->flash('error', 'Appointment Failed to Create, check your dates.');
        }

        $appointment->save();
        $request->session()->flash('success', 'Appointment Created');
        return $request->session()->flash('success', 'Appointment Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $loggedInUser = Auth::user();
      $user_id = $loggedInUser->id;
      $appointment = Appointment::find($id);
      if ($user_id != $appointment->user_id) {
        \Session::flash('error', 'You do not have permission to access this Appointment.');
        return redirect('ataglance')->with(['page' => 'Appointment']);
      }
      else {
        $artistName = User::findOrFail($appointment->user_id)->name;
        $images = $appointment->reference_json;
        // return view('appointments.show')->with('appointment', $appointment);
        return view('appointments.show', array('page' => 'Appointment', 'appointment'=>$appointment, 'artistName'=>$artistName, 'images'=>json_decode($images, true)));
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $appointment = Appointment::find($id);
      $images = $appointment->reference_json;
      $lengthofimages = count($images);
      return view('appointments.edit', array('page' => "Edit Appointment", 'appointment'=>$appointment, 'images'=>json_decode($images, true), 'lengthofimages' => $lengthofimages));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $appointment = Appointment::find($id);

      // return redirect('/test');
      $this->validate($request, [
        // 'clientid' => 'required',
        'title' => 'required',
        'description' => 'required',
        // 'clientname' => 'required',
        // 'clientphone' => 'required',
        'totalprice' => 'required',
        'cover_image' => 'image|nullable|max1999'
      ]);

      // if ($appointment->cover_image != "defaultimage") {
      //   $imageNamesToStore = [];
      //   $imageNamesToStore[] = json_decode($appointment->reference_json);
      // }
      // else if(($request->hasFile('file')) && ($appointment->cover_image != "defaultimage")){

      $appointment->user_id = auth()->user()->id;
      $appointment->client_id = $appointment->client_id;
      $appointment->title = $request->input('title');
      $appointment->description = $request->input('description');
      // $appointment->client_name = $request->input('clientname');
      // $appointment->client_phone = $request->input('clientphone');
      $appointment->total_cost = $request->input('totalprice');
      $appointment->deposit_paid = $request->input('deposit');
      $appointment->amount_to_pay = $request->input('amounttopay');
      $appointment->appointment_date = \Carbon\Carbon::parse($request->input('date'))->format('y/m/d');
      $appointment->start_time = $request->input('starttime');
      $appointment->end_time = $request->input('endtime');
      // $appointment->cover_image = $fileNameToStore;
      // $appointment->reference_json = json_encode($imageNamesToStore);
      $appointment->complete = 0;
      $appointment->canceled = 0;
      $appointment->refunded = 0;
      $appointment->save();

      $request->session()->flash('success', 'Appointment Updated');

      // return redirect('/')->with('success', 'Appointment Created');
      // return 'success';
      // return $request->session()->flash('success', 'Appointment Updated');
      return redirect('appointments/' . $appointment->id )->with(['page' => 'Appointments', 'success' => 'Appointment Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $appointment = Appointment::find($id);
      $appointment->delete();

      foreach ($appointments as $appointment) {
        $appointment->delete();
      }
      return redirect('appointments')->with(['page' => 'Appointments', 'success' => 'Appointment Deleted']);
    }



    public function markComplete($id) {
      $loggedInUser = Auth::user();

      $appointment = Appointment::find($id);

      if ($loggedInUser->id == $appointment->user_id) {
        $appointment->complete = 1;
        $appointment->canceled = 0;
        $appointment->refunded = 0;
        $appointment->save();

        return redirect('appointments/'.$id)->with(['page' => 'Appointments', 'success' => 'Appointment Completed']);
      }
      else {
        return redirect('appointments/'.$id)->with(['page' => 'Appointments', 'error' => 'You Do Not Own This Appointment']);
      }

    }
    public function markCanceled($id) {
      $loggedInUser = Auth::user();

      $appointment = Appointment::find($id);

      if ($loggedInUser->id == $appointment->user_id) {
        $appointment->complete = 0;
        $appointment->canceled = 1;
        $appointment->refunded = 0;
        $appointment->save();

        return redirect('appointments/'.$id)->with(['page' => 'Appointments', 'success' => 'Appointment Canceled']);
      }
      else {
        return redirect('appointments/'.$id)->with(['page' => 'Appointments', 'error' => 'You Do Not Own This Appointment']);
      }
    }
    public function markRefunded($id) {
      $loggedInUser = Auth::user();

      $appointment = Appointment::find($id);

      if ($loggedInUser->id == $appointment->user_id) {
        $appointment->complete = 0;
        $appointment->canceled = 0;
        $appointment->refunded = 1;
        $appointment->save();

        return redirect('appointments/'.$id)->with(['page' => 'Appointments', 'success' => 'Appointment Refunded']);
      }
      else {
        return redirect('appointments/'.$id)->with(['page' => 'Appointments', 'error' => 'You Do Not Own This Appointment']);
      }
    }


}
