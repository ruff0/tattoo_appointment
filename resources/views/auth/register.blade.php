<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('css/material-kit.css') }}" rel="stylesheet">
  <link href="{{ asset('css/material-landing.css') }}" rel="stylesheet">

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

  <script src="https://js.stripe.com/v3/"></script>
</head>

<body class="index-page">
<!-- Navbar -->
<nav class="navbar navbar-transparent navbar-fixed-top navbar-color-on-scroll">
	<div class="container">
        <div class="navbar-header">
	    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index">
	        	<span class="sr-only">Toggle navigation</span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	    	</button>
	    	<a href="{{ url('/') }}">
	        	<div class="logo-container">
  	          {{-- <div class="logo">
                <img src="{{URL::to('/')}}/images/landing-img/logo.png" alt="Creative Tim Logo" rel="tooltip" title="<b>Material Kit</b> was Designed & Coded with care by the staff from <b>Creative Tim</b>" data-placement="bottom" data-html="true">
              </div> --}}
    	        <div class="brand">
    	           {{ config('app.name', 'Laravel') }}
    	        </div>
				    </div>
	      	</a>
	    </div>

	    <div class="collapse navbar-collapse" id="navigation-index">
	    	<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="{{ route('login') }}">
						<i class="material-icons">perm_identity</i> Login
					</a>
				</li>
				<li>
					<a href="{{ route('register') }}">
						<i class="material-icons">unarchive</i> Register
					</a>
				</li>
				{{-- <li>
					<a rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank" class="btn btn-white btn-simple btn-just-icon">
						<i class="fa fa-twitter"></i>
					</a>
				</li>
				<li>
					<a rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank" class="btn btn-white btn-simple btn-just-icon">
						<i class="fa fa-facebook-square"></i>
					</a>
				</li>
				<li>
					<a rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank" class="btn btn-white btn-simple btn-just-icon">
						<i class="fa fa-instagram"></i>
					</a>
				</li> --}}

	    	</ul>
	    </div>
	</div>
</nav>
<!-- End Navbar -->

<div class="wrapper">
  <div class="header header-filter register-page" id="register-page" style="background-image: url('{{URL::to('/')}}/images/landing-img/header_bg.jpg'); height: 35vh;
    min-height: 0px;">
  </div>
  	<div class="main main-raised">
      @include('inc.messages')
      <div class="content">
        <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-default">
                  {{-- <div class="panel-heading">Register</div> --}}
                  <div class="panel-body">
                      {{-- <form class="form-horizontal" method="POST" action="{{ route('register') }}"> --}}
                        <form class="form-horizontal" action="{{ route('register') }}" method="post" id="payment-form">
                          {{ csrf_field() }}
                          <div class="form-group">
                            <label for="subscription-type" class="col-md-4 control-label">Select Subscription</label>
                            <div class="col-md-6">
                              <select class="form-control" name="subscription-type">
                                <option value="tattoobookingapp_plan_1">Monthly ($5 USD / Month)</option>
                                <option value="tattoobookingapp_plan_2">Yearly ($50 USD / Year)</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                              <label for="name" class="col-md-4 control-label">Name</label>

                              <div class="col-md-6">
                                  <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                  @if ($errors->has('name'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                              <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                              <div class="col-md-6">
                                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <label for="password" class="col-md-4 control-label">Password</label>

                              <div class="col-md-6">
                                  <input id="password" type="password" class="form-control" name="password" required>

                                  @if ($errors->has('password'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group">
                              <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                              <div class="col-md-6">
                                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                              </div>
                          </div>

                          <div class="form-group form-row">
                            <label for="card-element" class="col-md-4 control-label">
                              Credit or debit card
                            </label>
                            <div class="col-md-6" id="card-element">
                              <!-- a Stripe Element will be inserted here. -->
                            </div>

                          </div>
													<div class="form-group form-row">
														<label class="col-md-4 control-label">
														</label>
														  <div class="col-md-6">
															If you want to test the card payment use "4242 4242 4242 4242" as card number, with any date in future, any three numbers in CVC and any five numbers in ZIP.
														</div>
													</div>

                          <div class="form-group">
                              <div class="col-md-6 col-md-offset-4">
                                  <button type="submit" class="btn btn-primary">
                                      Register
                                  </button>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>
<footer class="footer">
  <div class="container">
      <nav class="pull-left">
          <ul>
      <li>
        <a href="http://400tres.com/profile/#contact-heading">
          Contact Me
        </a>
      </li>
      <li>
        <a href="{{ route('login') }}">
           Login
        </a>
      </li>
      <li>
        <a href="{{ route('register') }}">
           Register
        </a>
      </li>
      <li>
        <a href="http://facebook.com/objectsinmirrorarecloserthantheyappear">
          Facebook
        </a>
      </li>
          </ul>
      </nav>
      <div class="copyright pull-right">
          &copy; 2017, made by <a href="http://400tres.com/">400tres.com</a>
      </div>
  </div>
</footer>
</body>

<script type="text/javascript">
  var stripe = Stripe('pk_test_e1cqil1IMEmjrMP2Ma91ibIa');
  var elements = stripe.elements();
  // Custom styling can be passed to options when creating an Element.
  var style = {
    base: {
      // Add your base input styles here. For example:
      fontSize: '16px',
      lineHeight: '24px'
    }
  };

  // Create an instance of the card Element
  var card = elements.create('card', {style: style});

  // Add an instance of the card Element into the `card-element` <div>
  card.mount('#card-element');

  card.addEventListener('change', function(event) {
    var displayError = document.getElementById('card-errors');
    if (event.error) {
      displayError.textContent = event.error.message;
    } else {
      displayError.textContent = '';
    }
  });


  // Create a token or display an error when the form is submitted.
  var form = document.getElementById('payment-form');
  form.addEventListener('submit', function(event) {
    event.preventDefault();

    stripe.createToken(card).then(function(result) {
      if (result.error) {
        // Inform the user if there was an error
        var errorElement = document.getElementById('card-errors');
        errorElement.textContent = result.error.message;
      } else {
        // Send the token to your server
        stripeTokenHandler(result.token);
      }
    });
  });

  function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Submit the form
    form.submit();
  }
</script>

<!--   Core JS Files   -->
<script src="{{URL::to('/')}}/js/landing-js/jquery.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/js/landing-js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{URL::to('/')}}/js/landing-js/material.min.js"></script>

<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{URL::to('/')}}/js/landing-js/nouislider.min.js" type="text/javascript"></script>

<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
<script src="{{URL::to('/')}}/js/landing-js/bootstrap-datepicker.js" type="text/javascript"></script>

<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="{{URL::to('/')}}/js/landing-js/material-kit.js" type="text/javascript"></script>

<script type="text/javascript">

  $().ready(function(){
    // the body of this function is in assets/material-kit.js
    materialKit.initSliders();
          window_width = $(window).width();

          if (window_width >= 992){
              big_image = $('.wrapper > .header');

      $(window).on('scroll', materialKitDemo.checkScrollForParallax);
    }

  });
</script>

</body>
