@extends('layouts.app')
  @section('content')
    <div class="wrapper">
    @include('inc.backsidebar')
    <div class="main-panel">
      @include('inc.nav')
      @include('inc.messages')
      @if ( $appointment->cover_image != "defaultimage.jpg")
      <div class="page-header appointment-header" id="appointment-header" style="background-image:url('{{ URL::to('/') }}/storage/img/appointment_imgs/cover_image/{{$appointment->cover_image}}');">
      @else
      <div class="page-header appointment-header appointment-header-default" id="appointment-header" style="background-image:url('{{ URL::to('/') }}/storage/img/defaultimage.jpg');">
      @endif
        <div class="container">
          <div class="overlay">
            <h1>{{$appointment->title}} - For, {{$appointment->customer->f_name}}</h1>
            <span>Artist: {{$artistName}}</span>
            <a href="tel:{{$appointment->customer->phone_no}}" class="pull-right"><span class="glyphicon glyphicon-phone"></span> {{$appointment->customer->phone_no}}</a>
          </div>
        </div>
      </div>
        <div class="container">
          @include('inc.messages')
          <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
              @if ( $appointment->complete == 1)
                <h3>Appointment Complete</h3>
              @elseif ($appointment->canceled == 1)
                <h3>Appointment Canceled</h3>
              @elseif ($appointment->refunded == 1)
                <h3>Appointment Refunded</h3>
              @else
                <a href="{{url('appointments/markComplete/'.$appointment->id)}}" class="btn btn-success pull-left appointment-control" id="complete">Complete</a>
                <a href="{{url('appointments/markCanceled/'.$appointment->id)}}" class="btn btn-warning appointment-control" id="canceled">Cancel</a>
                <a href="{{url('appointments/markRefunded/'.$appointment->id)}}" class="btn btn-danger pull-right appointment-control" id="refund">Refund</a>
              @endif
            </div>
          </div>
          <a href="{{url('appointments/'.$appointment->id. '/edit')}}" class="btn btn-primary appointment-control" id="edit">Edit</a>
        </div>
      <div class="container">
        @if ( $appointment->cover_image != "defaultimage.jpg")
          <div class="col-lg-7">
            <h2>About the Appointment</h2>
            <h3>Date &amp; Time</h3>
            <span><em>Date: </em> {{$appointment->appointment_date}}</span>
            <span class="will th"><em>Time: </em> {{substr($appointment->start_time, 0, -3)}} - {{substr($appointment->end_time, 0, -3)}}</span>
            <h3>Description</h3>
            <p>{{$appointment->description}}</p>
            <h3>Pricing</h3>
            <p><em>Total:</em> €{{$appointment->total_cost}}</p>
            <p><em>Deposit:</em> €{{$appointment->deposit_paid}}</p>
            <p><em>Amount To Pay:</em> €{{$appointment->amount_to_pay}}</p>
          </div>
          <div class="col-lg-5">
            <div class="slick-slider slick-right">
              @foreach ($images as $image)
                <div class="slick-slider-image">
                  <a href="#"></a>
                  @if ( $image != end($images))
                    <div class="slick-slider-img" style="background-image:url('{{ URL::to('/') }}/storage/img/appointment_imgs/{{$image}}');" src="{{ URL::to('/') }}/storage/img/appointment_imgs/{{$image}}"></div>
                  @else
                    <div class="slick-slider-img" style="background-image:url('{{ URL::to('/') }}/storage/img/appointment_imgs/cover_image/{{$image}}');" src="{{ URL::to('/') }}/storage/img/appointment_imgs/cover_image/{{$image}}"></div>
                  @endif
                </div>
              @endforeach
            </div>
          </div>
          <div class="slick-slider-nav">
            @foreach ($images as $image)
              @if ( $image != end($images))
                <div class="slick-slider-nav-img" style="background-image:url('{{ URL::to('/') }}/storage/img/appointment_imgs/{{$image}}');"></div>
              @else
                <div class="slick-slider-nav-img" style="background-image:url('{{ URL::to('/') }}/storage/img/appointment_imgs/cover_image/{{$image}}');"></div>
              @endif
            @endforeach
          </div>
        @else
          <div class="col-lg-7">
            <h2>About the Appointment</h2>
            <h3>Date &amp; Time</h3>
            <span><em>Date: </em> {{$appointment->appointment_date}}</span>
            <span class="will th"><em>Time: </em> {{$appointment->start_time}} - {{$appointment->end_time}}</span>
            <h3>Description</h3>
            <p>{{$appointment->description}}</p>
            <h3>Pricing</h3>
            <p><em>Total:</em> €{{$appointment->total_cost}}</p>
            <p><em>Deposit:</em> €{{$appointment->deposit_paid}}</p>
            <p><em>Amount To Pay:</em> €{{$appointment->amount_to_pay}}</p>
          </div>
        @endif
      </div>
    </div>
  </div>
      <script>
      $(".appointment-control").on("click", function(){
        if (this.id == "complete")
        return confirm("Are you sure you want to mark this as appointment Complete");
        else if (this.id == "canceled")
        return confirm("Are you sure you want to mark this as appointment Canceled");
        else if (this.id == "refund")
        return confirm("Are you sure you want to mark this as appointment Refunded");
      });
      </script>

  {{-- <img src="{{ URL::to('/') }}/storage/img/appointment_imgs/cover_image/{{$appointment->cover_image}}" alt=""> --}}

@endsection
