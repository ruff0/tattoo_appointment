<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class PagesController extends Controller
{
  public function index() {
    if (Auth::guest()) {
      return view('pages.index');
    }
    else {
      return redirect('/ataglance');
    }
  }

}
