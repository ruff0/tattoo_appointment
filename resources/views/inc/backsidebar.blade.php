<div class="sidebar" data-color="blue" data-image="../images/dashboard-img/sidebar-5.jpg">

<!--

    Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
    Tip 2: you can also add an image using data-image tag

-->

  <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{ url('/') }}" class="simple-text">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>
        <ul class="nav">
            <li class="">
                <a href="{{ URL('/') }}">
                    <i class="pe-7s-home"></i>
                    <p>Home</p>
                </a>
            </li>
            <li class="active">
                <a href="{{ URL::previous() }}">
                    <i class="pe-7s-angle-left-circle"></i>
                    <p>Back</p>
                </a>
            </li>
        </ul>
  </div>
</div>
