<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model {
  public function customer() {
    return $this->belongsTo('App\Customer','client_id');
  }
}
