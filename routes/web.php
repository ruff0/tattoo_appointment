<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;

Auth::routes();
Route::group(['middleware' => 'auth'], function(){
  Route::get('settings', array('uses'=>'SubscriptionController@settingspage'));

  Route::get('register/planselect',array('uses'=>'Auth\RegisterController@planselect'));


  Route::group(['before' => 'subscribed'], function(){
    Route::get('subscription/cancel', ['before' => 'not.canclled|csrf', 'uses' => 'SubscriptionController@getCancel']);
    Route::get('subscription/resume', ['before' => 'canclled|csrf', 'uses' => 'SubscriptionController@getResume']);
    Route::get('subscription/card', ['uses' => 'SubscriptionController@getCard']);
    Route::post('subscription/card', ['before' => 'csrf', 'uses' => 'SubscriptionController@postCard']);
  });

  Route::post('stripe/webhook', '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook');

  Route::group(['middleware' => 'subscription'], function(){
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/ataglance', 'AtAGlanceController@index');@
    Route::get('/earnings', 'EarningsController@index');
    Route::resource('appointments', 'AppointmentController');
    // Route::get('/earnings', 'EarningsController@index');
    Route::resource('customers', 'CustomerController');

    Route::post('/dashboard/get/weekly-earnings', 'DashboardController@getEarningsForWeek');
    Route::post('/dashboard/get/monthly-earnings', 'DashboardController@getEarningsForMonth');
    Route::post('/dashboard/get/yearly-earnings', 'DashboardController@getEarningsForYear');


    Route::get('/ajax/appointments', function() {
      $today = date('Y-m-d');
      // $appointments = Appointment::all();
      $loggedInUser = Auth::user()->id;
      $appointments = DB::table('appointments')
        ->where('user_id', $loggedInUser)
        ->where('appointment_date', '>=', $today)
        ->orderBy('appointment_date', 'asc')
        ->paginate(8);
      foreach ($appointments as $appointment) {
        $client = DB::table('customers')
          ->where('id', $appointment->client_id)
          ->get();
        $name;
        foreach ($client as $cli) {
          $name = $cli->f_name;
        }
        $appointment->client_name = $name;
      }
      return View::make('appointments', ['page'=> 'At a Glance', 'appointments' => $appointments])->render();
    });



    Route::get('/ajax/customers', function() {
      $loggedInUser = Auth::user();
      $user_id = $loggedInUser->id;
      $customers = \App\Customer::where('user_id', $user_id)->paginate(10);
      foreach ($customers as $customer) {
        $customer->dob = date_diff(date_create($customer->dob), date_create('today'))->y;
      }
      return View::make('customers', ['page'=> 'Customers', 'customers' => $customers])->render();
    });
    Route::get('/ajax/earnings', function() {
      $loggedInUser = Auth::user();
      $user_id = $loggedInUser->id;
      $appointments = \App\Appointment::where('user_id', $user_id)->paginate(10);
      // return view('customers/index', ['page'=> 'Customers', 'customers' => $customers]);
      return View::make('earnings')->with(['page' => 'Earnings', 'appointments' => $appointments]);
      // return View::make('customers', ['page'=> 'Customers', 'customers' => $customers])->render();
    });
    // Route::get('/ajax/customersearch', function() {
    //   $customers = \App\Customer::paginate(10);
    //   foreach ($customers as $customer) {
    //     $customer->dob = date_diff(date_create($customer->dob), date_create('today'))->y;
    //   }
    //   return View::make('customers', ['page'=> 'Customers', 'customers' => $customers])->render();
    // });
    Route::get('/ajax/customersearch',array('as'=>'customersearch','uses'=>'SearchController@customersearch'));
    Route::get('/ajax/earningssearch',array('as'=>'earningssearch','uses'=>'SearchController@earningssearch'));



    Route::get('search',array('as'=>'search','uses'=>'SearchController@search'));
    Route::get('autocomplete',array('as'=>'autocomplete','uses'=>'SearchController@autocomplete'));

    // route::get('/getExport', 'ExcelController@getExport');
    Route::get('/getExport',array('uses'=>'ExcelController@getExport'));

    Route::get('appointments/markComplete/{id}',array('uses'=>'AppointmentController@markComplete'));
    Route::get('appointments/markCanceled/{id}',array('uses'=>'AppointmentController@markCanceled'));
    Route::get('appointments/markRefunded/{id}',array('uses'=>'AppointmentController@markRefunded'));


    // Route::get('signup',array('uses'=>'SubscriptionController@index'));

  });
});

Route::get('/', 'PagesController@index');
