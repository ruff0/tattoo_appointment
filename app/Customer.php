<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Customer extends Model {
  public $fillable = ['f_name', 'l_name'];

  public function Appointment() {
    return $this->hasMany('App\Appointment');
  }
}
