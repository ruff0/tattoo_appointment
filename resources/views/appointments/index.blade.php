@extends('layouts.app')
  @section('content')
    <div class="wrapper">
    @include('inc.sidebar')
    <div class="main-panel">
      @include('inc.nav')
      @include('inc.messages')
      <div class="content">
        <div class="row text-center">
          <button type="button" id="newAppointmentBtn" class="btn btn-primary" data-toggle="modal" data-target="#newAppointmentModal">
            New Appointment
          </button>
        </div>
          <div class="container-fluid">
        {!! $calendar->calendar() !!}
        {!! $calendar->script() !!}
      </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="newAppointmentModal"
  tabindex="false" role="dialog"
  aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"
        data-dismiss="modal"
        aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"
        id="favoritesModalLabel">New Appointment</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(['action' => 'AppointmentController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class'=>'dropzone', 'id'=>"mydropzone", 'data-toggle'=>'validator']) !!}
        <div class="form-group">
          {{Form::label('clientid', 'Customer Name')}}
          {{Form::select('clientid', [], null, ['class'=>'select2 form-control', 'placeholder' => 'Customer Name'])}}
        </div>
        <script type="text/javascript">
          $(document).ready(function() {
            var path = "{{ route('autocomplete') }}";
            $(".select2").select2({
              width: '100%',
              ajax: {
                url: path,
                dataType: 'json',
                type: "GET",
                delay: 250,
                // quietMillis: 50,
                data: function (params) {
                  return {
                    q: params.term, // search term
                  };
                },
                processResults: function (data) {
                  return {
                    results: $.map(data.items, function (item){
                      return {
                        id:item.id,
                        text:item.f_name + " " + item.l_name
                      };
                    })
                  }
                },
                cache: true,
              },
              });
            });
        </script>
        <div class="form-group">
          {{-- Title --}}
          {{Form::label('title', 'Title')}}
          {{Form::text('title', '', ['class'=>'form-control', 'placeholder'=>'title', 'required'])}}
          <div class="help-block with-errors"></div>
        </div>

        <div class="form-group">
          {{Form::label('date', 'Date')}}
          {{Form::text('date', '', ['id'=>'datePicker'])}}
      <div class="row">
        <div class="col-sm-6 pull-left">
          {{Form::label('starttime', 'Start Time')}}<br>
          {{Form::text('starttime', '', ['id'=>'startTimePicker'])}}
        </div>
        <div class="col-sm-6 pull-right">
          {{Form::label('endtime', 'End Time')}}<br>
          {{Form::text('endtime', '', ['id'=>'endTimePicker'])}}
        </div>
      </div>
    </div>
    <script type="text/javascript">
    $(function () {

      $('#datePicker').datetimepicker({
        inline: true,
        sideBySide: true,
        format : 'YYYY/MM/DD',
        minDate:new Date()

      });
      $('#startTimePicker').datetimepicker({
        inline: true,
        sideBySide: true,
        format : 'HH:mm',
        // enabledHours: [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22]
      });
      $('#endTimePicker').datetimepicker({
        inline: true,
        sideBySide: true,
        format : 'HH:mm'
      });
    });
  </script>

  <div class="form-group">
    {{-- Description --}}
    {{Form::label('description', 'Description')}}
    {{Form::textArea('description', '', ['class'=>'form-control', 'placeholder'=>'description'])}}
    <div class="help-block with-errors"></div>
  </div>
  <div class="form-group">
    {{-- Client Info --}}
    {{-- {{Form::label('clientname', 'Client Name')}} --}}
    {{-- {{Form::text('clientname', '', ['class'=>'form-control', 'placeholder'=>'Client Name', 'required'])}} --}}
    {{-- <div class="help-block with-errors"></div> --}}
  </div>
  <div class="form-group">
    {{-- {{Form::label('clientphone', 'Phone No')}} --}}
    {{-- {{Form::tel('clientphone', '', ['class'=>'form-control', 'placeholder'=>'Phone Number', 'pattern'=>'^[0-9]*$', 'required'])}} --}}
    {{-- <div class="help-block with-errors"></div> --}}
  </div>
  <div class="form-group">
    {{Form::label('totalprice', 'Total Price')}}
    {{Form::number('totalprice', '', ['class'=>'form-control', 'value'=>'null', 'required'])}}
    <div class="help-block with-errors"></div>
  </div>
  <div class="form-group">
    {{Form::label('deposit', 'Deposit Paid')}}
    {{Form::number('deposit', '', ['class'=>'form-control',  'value'=>'null', 'required'])}}
    <div class="help-block with-errors"></div>
  </div>
  <div class="form-group">
    {{Form::label('amounttopay', 'Amount to Pay')}}
    {{Form::number('amounttopay', '', ['class'=>'form-control', 'readonly'])}}
  </div>
  <div class="form-group">
    {{-- References --}}
    {{Form::label('file', 'Upload Multiple Reference Meterial')}}
  </div>

</div>
<div class="modal-footer">
  <button type="button"
  class="btn btn-default"
  data-dismiss="modal">
  Close
</button>
<span class="pull-right">
  {{Form::submit('Submit', ['class'=>'btn btn-primary', 'id'=>'sbmtbtn'])}}
</span>
</div>
{!! Form::close() !!}
</div>
</div>
</div>
    @endsection

{{-- </div> --}}
