@if (count($customers) == 0)
<div class="content table-responsive table-full-width">
  <h2 class="text-center">No Results.</h2>
</div>
@else
<div class="content table-responsive table-full-width">
    <table class="table table-hover table-striped">
        <thead>
          <th>View</th>
          <th>Name</th>
          <th>Surname</th>
          <th>Address</th>
          <th>Email</th>
          <th>Age</th>
          <th>ID</th>
          <th>Edit</th>
          <th>Delete</th>
        </thead>
        <tbody>
          @foreach ($customers as $customer)
            <tr>
              <td><a href="customers/{{$customer->id}}">View</a></td>
              <td>{{$customer->f_name}}</td>
              <td>{{$customer->l_name}}</td>
              <td>{{$customer->address_line_1}}, {{$customer->address_line_2}}, {{$customer->city}}, {{$customer->country}}</td>
              <td>{{$customer->email}}</td>
              <td>{{$customer->dob}}</td>
              <td>{{$customer->image_url}}</td>
              <td><a href="/customers/{{$customer->id}}/edit"><i class="pe-7s-pen"></i></a></td>
              <td>
                {!!Form::open(['action' => ['CustomerController@destroy', $customer->id], 'method' => 'POST', 'class' => 'delete'])!!}
                  {{Form::hidden('_method', 'DELETE')}}
                  {{-- {{Form::submit('Delete', ['class' => 'delete'])}} --}}
                  {{ Form::button('<i class="pe-7s-delete-user"></i>', ['class' => 'btn btn-danger', 'role' => 'button', 'type' => 'submit']) }}
                {!!Form::close()!!}
                {{-- <a href="/customers/delete" class="delete"><i class="pe-7s-delete-user"></i></a> --}}
              </td>
            </tr>
          @endforeach
        </tbody>
    </table>
    <div class="footer text-center">
      {{ $customers->links() }}
    </div>
</div>
<script>
    $(".delete").on("submit", function(){
        return confirm("Do you want to delete this customer and all related appointments?");
    });
</script>
@endif
