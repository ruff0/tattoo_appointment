<div class="sidebar" data-color="blue" data-image="images/dashboard-img/sidebar-5.jpg">

<!--

    Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
    Tip 2: you can also add an image using data-image tag

-->

  <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{ url('/') }}" class="simple-text">
                {{ config('app.name', 'InkCalendar') }}
            </a>
        </div>
        <ul class="nav">
            <li @if ($page == "At a Glance")
              class="active"
            @endif>
                <a href="ataglance">
                    <i class="pe-7s-glasses"></i>
                    <p>At a Glance</p>
                </a>
            </li>
            <li @if ($page == "Appointments")
              class="active"
            @endif>
                <a href="appointments">
                    <i class="pe-7s-notebook"></i>
                    <p>Appointments</p>
                </a>
            </li>
            <li @if ($page == "Earnings")
              class="active"
            @endif>
                <a href="earnings">
                    <i class="pe-7s-piggy"></i>
                    <p>Earnings</p>
                </a>
            </li>
            <li @if ($page == "Customers")
              class="active"
            @endif>
              <a href="customers">
                <i class="pe-7s-users"></i>
                <p>Customers</p>
              </a>
            </li>
            {{-- <li @if ($page == "User Profile")
              class="active"
            @endif>
                <a href="user">
                    <i class="pe-7s-user"></i>
                    <p>User Profile</p>
                </a>
            </li> --}}
        </ul>
  </div>
</div>
