<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

class AtAGlanceController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $loggedInUser = Auth::user();
    $user_id = $loggedInUser->id;
    $issubscribed = "";
    if ($loggedInUser->subscribed('main')) {
      if ($loggedInUser->subscription('main')->cancelled()){
        $issubscribed = "Subscription Cancelled";
      }
      else{
        $issubscribed = "Subscribed";
      }
    }
    else {
      $issubscribed = "Not! Subscribed";
    }


    $today = date('Y-m-d');
    // $appointments = Appointment::all();
    $appointments = DB::table('appointments')
      ->where('user_id', $user_id)
      ->where('appointment_date', '>=', $today)
      ->orderBy('appointment_date', 'asc')
      ->paginate(8);
    foreach ($appointments as $appointment) {
      $client = DB::table('customers')
        ->where('id', $appointment->client_id)
        ->get();
      $name;
      foreach ($client as $cli) {
        $name = $cli->f_name;
      }
      $appointment->client_name = $name;
    }

    return view('ataglance', ['page'=> 'At a Glance', 'appointments' => $appointments, 'subscribed' => $issubscribed]);
  }
}
