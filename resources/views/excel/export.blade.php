<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Earnings Info</title>
  </head>
  <body>
    @if (count($appointments) == 0)
    <div class="content table-responsive table-full-width">
      <h2 class="text-center">No Results.</h2>
    </div>
    @else
        <table id="earnings-table" class="table table-hover table-striped">
            <thead>
              <tr>
                <th>€ Earnings</th>
                <th>Status</th>
                <th>Title</th>
                <th>Date</th>
                <th>For (Name)</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($appointments as $appointment)
                <tr>
                  @if ($appointment->complete)
                    <td>€{{$appointment->total_cost}}</td>
                    <td>Complete</td>
                  @elseif ($appointment->canceled)
                    <td>€{{$appointment->deposit_paid}}</td>
                    <td>Cancled</td>
                  @elseif ($appointment->refunded)
                    <td>€0</td>
                    <td>Refunded</td>
                  @else
                    <td>€{{$appointment->total_cost}}</td>
                    <td>Status Not Marked</td>
                  @endif
                  <td>{{$appointment->title}}</td>
                  <td>{{$appointment->appointment_date}}</td>
                  <td>{{$appointment->customer->f_name}} {{$appointment->customer->l_name}}</td>
                </tr>
              @endforeach
            </tbody>
        </table>
        <div class="footer text-center">
          {{-- {{ $appointments->links() }} --}}
        </div>
      @endif
  </body>
</html>
