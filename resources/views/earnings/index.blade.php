@extends('layouts.app')
@section('content')
<div class="wrapper">
  @include('inc.sidebar')
  <div class="main-panel">
    @include('inc.nav')
    @include('inc.messages')
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="header">
                  <h4 class="title">Earnings So Far</h4>
                  <p class="category">Earning and Projected Earnings</p>
                </div>
                <div class="content">
                  <div id="chartPreferences" class="ct-chart ct-perfect-fourth"></div>
                  {{-- <hr> --}}
                  <div class="footer">
                    <div class="legend">
                      {{-- <i class="fa fa-circle text-info"></i> Open --}}
                    </div>
                    <div class="chart-controls pull-right">
                      <span class="btn btn-sm btn-primary" id="chart-week">Week</span>
                      {{-- <span class="btn btn-sm btn-primary" id="chart-month">Month</span> --}}
                      <span class="btn btn-sm btn-primary" id="chart-year">Year</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="header">
                      <h4 class="title">Search Earnings</h4>
                      <p class="category">Search in Range</p>
                      <div class="row">
                        {{-- 'data-toggle'=>'validator' --}}
                        {!! Form::open(['action' => 'ExcelController@getExport', 'method' => 'GET', 'enctype' => 'multipart/form-data']) !!}
                          <div class="col-sm-4">
                            <div class="form-group">
                              {{-- Title --}}
                              {{Form::label('startDate', 'Start Date')}}
                              {{Form::text('startDate', '', ['id'=>'datePicker', 'class'=>'form-control search-control'])}}
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              {{-- Client Info --}}
                              {{Form::label('endDate"', 'End Date')}}
                              {{Form::text('endDate', '', ['id'=>'datePicker_2', 'class'=>'form-control search-control'])}}
                            </div>
                          </div>
                          <div class="form-group" id="searchearningsbtn">
                            {{Form::submit('Download CSV', ['class'=>'pull-right btn btn-primary', 'id'=>'sbmtbtn'])}}
                          </div>
                        {!! Form::close() !!}
                      </div>
                        {{-- <div class="form-group">
                          {{Form::label('file', 'Upload Multiple Reference Meterial')}}
                        </div> --}}
                        {{-- <p class="category">Here is a subtitle for this table</p> --}}
                      </div>
                      <div id="earnings">
                        @include('earnings')
                      </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(function () {
    $('#datePicker').datetimepicker({
      inline: true,
      sideBySide: true,
      format : 'YYYY/MM/DD'
    });
    $('#datePicker_2').datetimepicker({
      inline: true,
      sideBySide: true,
      format : 'YYYY/MM/DD'
    });
  });
  var delay = (function(){
    var timer = 0;
    return function(callback, ms){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
    };
  })();
  $(document).on('click', '.pagination a',function(e){
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    getEarnings(page);
  });
  function getEarnings(page) {
    console.log('getting earnings for page =' + page);
    var startDate = "";
    var endDate = "";
    $('input.search-control').each(function(index, value){
      // console.log("working");
      if (index == 0) {
        startDate = $(this).val();
      }
      if (index == 1) {
        endDate = $(this).val();
      }
    });
    $.ajax({
      url: "ajax/earningssearch?startDate=" + startDate + "&endDate=" + endDate + "&page=" + page
      // url: 'ajax/earnings?page='+page
    }).done(function(data){
      // console.log(data);
      $('#earnings').html(data);
    });
  }

  $('.search-control').on('dp.change', function(e){
    // e.preventDefault();
    var startDate = "";
    var endDate = "";
    $('input.search-control').each(function(index, value){
      // console.log("working");
      if (index == 0) {
        startDate = $(this).val();
      }
      if (index == 1) {
        endDate = $(this).val();
      }
      // console.log("Name" + ": " + name + "\nEmail" + ": " + email);
    });
    console.log(startDate + " to " + endDate);
    // delay(function(){
    //   if ((name == "") && (email == "")) {
    //     getCustomers(1);
    //   }
    //   else {
    //     getCustomersSearch(name, email);
    //   }
    // }, 500 );
    // getCustomersSearch();
    getEarningsSearch(startDate, endDate);
  });
  function getEarningsSearch(startDate, endDate) {
    var searchURL = "ajax/earningssearch?";
    searchURL = "ajax/earningssearch?startDate=" + startDate + "&endDate=" + endDate;
    $.ajax({
      url: searchURL
    }).done(function(data){
      $('#earnings').html(data);
    });
  }
</script>
@endsection
