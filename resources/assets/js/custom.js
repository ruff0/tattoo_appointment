//=========================================================
//
//    Slider
//
//=========================================================
document.addEventListener('DOMContentLoaded', function() {
  $('.slick-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slick-slider-nav'
  });
  $('.slick-slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slick-slider',
    dots: true,
    centerMode: true,
    arrows: true,
    focusOnSelect: true
  });
  $('.slick-slider').slickLightbox({
    src: 'src',
    itemSelector: '.slick-slider-img'
  });



  //=========================================================
  //
  //    Autocomplete
  //
  //=========================================================





//=========================================================
//
//    Chartist!!!
//
//=========================================================
  //Event listeners
  document.getElementById('chart-week').addEventListener('click', setChart);
  // document.getElementById('chart-month').addEventListener('click', setChart);
  document.getElementById('chart-year').addEventListener('click', setChart);

  var chartDataArray = [];
  var chartLableArray = [];
  var chartCount = 0;
  var chart;
  var period = "weekly-earnings";

  chart = new Chartist.Line('#chartPreferences', {
    labels: ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'],
    series: []
  }, {
    fullWidth: true,
    chartPadding: {
      right: 40
    }
  });

  getChartData();

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });


  function getChartData() {
      $.ajax({
          type: "POST",
          url: './dashboard/get/' + period,
          data: "",
          success: function(result) {
            setChartDataArray(result);
            console.log(result);
            reDrawChart();
          }
      })
  };

  function formatDate(date, type) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        dayName = '' + d.getDay(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    if (type == "full") {
      return [year, month, day].join('-');
    }
    if (type == "year") {
      return year;
    }
    if (type == "month") {
      return month;
    }
    if (type == "day") {
      return day;
    }
    if (type == "dayName") {
      if (dayName == 0) return "Sun";
      if (dayName == 1) return "Mon";
      if (dayName == 2) return "Tue";
      if (dayName == 3) return "Wed";
      if (dayName == 4) return "Thur";
      if (dayName == 5) return "Fri";
      if (dayName == 6) return "Sat";
    }
  }

  function setChartDataArray(result) {
    console.log(result.length);
    var length = result.length;
    var points = [];
    var labels = [];

    // if (period == "weekly-earnings") {
    //   labels = ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
    //   var currentDate = new Date;
    //   var j = 0;
    //   console.log(formatDate(currentDate, "full"));
    //   var dayAmount = 0;
    //   for (var i = 0; i <= 6; i++) {
    //     dayAmount = 0;
    //     for (var j = 0; j < result.length; j++) {
    //       if (result[j].appointment_date == formatDate(currentDate, "full")) {
    //         dayAmount = +dayAmount + +result[j].total_cost;
    //       }
    //     }
    //     points.push(dayAmount);
    //     currentDate.setDate(currentDate.getDate()+1);
    //   }
    // }

    if (period == "weekly-earnings") {
      // result.splice(0,1);
      result.replace(']',"");
      var points = result.split(',');
      points[0] = points[0].replace('[', "");
      points[points.length-1] = points[points.length-1].replace(']', "");
      labels = ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
    }


    if (period == "yearly-earnings") {
      // result.splice(0,1);
      result.replace(']',"");
      var points = result.split(',');
      points[0] = points[0].replace('[', "");
      points[points.length-1] = points[points.length-1].replace(']', "");
      labels = ['Jan', 'Feb', 'Mar', 'Arp', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    }


    chartDataArray = points;
    chartLableArray = labels;
  }

  function setChart() {
    var data = {
      labels: [],
      series: []
    }

    if (this.id === "chart-week") {
      period = "weekly-earnings";
      getChartData();
    }
    else if (this.id === "chart-month") {
      period = "monthly-earnings";
      getChartData();
    }
    else if (this.id === "chart-year") {
      period = "yearly-earnings";
      getChartData();
    }
  }



  function reDrawChart() {
    var data = {
      labels: [],
      series: []
    }

    if (this.id === "chart-week") {
      data = {
        labels: chartLableArray,
        series: [chartDataArray]
      }
    }
    else if (this.id === "chart-month") {
      for (var i = 0; i < chartDataArray.length; i++) {
        console.log(chartDataArray[i]);
      }
      data = {
        labels: chartLableArray,
        series: [chartDataArray]
      }
    }
    else if (this.id === "chart-year") {
      data = {
        labels: chartLableArray,
        series: [chartDataArray]
      }
    }
    else {
      // period = "weekly-earnings";
      data = {
        labels: chartLableArray,
        series: [chartDataArray]
      }
    }
    chart.update(data);
    setChartMarker();
  }


  function setChartMarker() {
    var d = new Date();
    var dayNumber = d.getDay();
    var monthNumber = d.getMonth();
    var lines;
    var line;
    if (period == "weekly-earnings") {
      lines = document.querySelectorAll(".ct-grid.ct-horizontal");

      for (i=0; i<lines.length; i++) {
        line = lines[i];
        if (i>=dayNumber) {
          line.style.stroke = "orangered";
        }
      }
      // line.style.stroke = "";
    }

    if (period == "yearly-earnings") {
      lines = document.querySelectorAll(".ct-grid.ct-horizontal");

      for (i=0; i<lines.length; i++) {
        line = lines[i];
        if (i>=monthNumber) {
          line.style.stroke = "orangered";
        }
      }
      // line.style.stroke = "";
    }



    // if (period == "yearly-earnings") {
    //   // result.splice(0,1);
    //   result.replace(']',"");
    //   var points = result.split(',');
    //   points[0] = points[0].replace('[', "");
    //   points[points.length-1] = points[points.length-1].replace(']', "");
    //   labels = ['Jan', 'Feb', 'Mar', 'Arp', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    // }
  }



  chart.on('draw', function(data) {
    if(data.type === 'line' || data.type === 'area') {
      data.element.animate({
        d: {
          begin: 1000 * data.index,
          dur: 1000,
          from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
          to: data.path.clone().stringify(),
          easing: Chartist.Svg.Easing.easeOutQuint
        }
      });
    }
    setChartMarker();
  });

}, false);





//=========================================================
//
//    Export Table!!!
//
//=========================================================






/*!
 * Validator v0.11.9 for Bootstrap 3, by @1000hz
 * Copyright 2017 Cina Saffary
 * Licensed under http://opensource.org/licenses/MIT
 *
 * https://github.com/1000hz/bootstrap-validator
 */

+function(a){"use strict";function b(b){return b.is('[type="checkbox"]')?b.prop("checked"):b.is('[type="radio"]')?!!a('[name="'+b.attr("name")+'"]:checked').length:b.is("select[multiple]")?(b.val()||[]).length:b.val()}function c(b){return this.each(function(){var c=a(this),e=a.extend({},d.DEFAULTS,c.data(),"object"==typeof b&&b),f=c.data("bs.validator");(f||"destroy"!=b)&&(f||c.data("bs.validator",f=new d(this,e)),"string"==typeof b&&f[b]())})}var d=function(c,e){this.options=e,this.validators=a.extend({},d.VALIDATORS,e.custom),this.$element=a(c),this.$btn=a('button[type="submit"], input[type="submit"]').filter('[form="'+this.$element.attr("id")+'"]').add(this.$element.find('input[type="submit"], button[type="submit"]')),this.update(),this.$element.on("input.bs.validator change.bs.validator focusout.bs.validator",a.proxy(this.onInput,this)),this.$element.on("submit.bs.validator",a.proxy(this.onSubmit,this)),this.$element.on("reset.bs.validator",a.proxy(this.reset,this)),this.$element.find("[data-match]").each(function(){var c=a(this),d=c.attr("data-match");a(d).on("input.bs.validator",function(){b(c)&&c.trigger("input.bs.validator")})}),this.$inputs.filter(function(){return b(a(this))&&!a(this).closest(".has-error").length}).trigger("focusout"),this.$element.attr("novalidate",!0)};d.VERSION="0.11.9",d.INPUT_SELECTOR=':input:not([type="hidden"], [type="submit"], [type="reset"], button)',d.FOCUS_OFFSET=20,d.DEFAULTS={delay:500,html:!1,disable:!0,focus:!0,custom:{},errors:{match:"Does not match",minlength:"Not long enough"},feedback:{success:"glyphicon-ok",error:"glyphicon-remove"}},d.VALIDATORS={"native":function(a){var b=a[0];return b.checkValidity?!b.checkValidity()&&!b.validity.valid&&(b.validationMessage||"error!"):void 0},match:function(b){var c=b.attr("data-match");return b.val()!==a(c).val()&&d.DEFAULTS.errors.match},minlength:function(a){var b=a.attr("data-minlength");return a.val().length<b&&d.DEFAULTS.errors.minlength}},d.prototype.update=function(){var b=this;return this.$inputs=this.$element.find(d.INPUT_SELECTOR).add(this.$element.find('[data-validate="true"]')).not(this.$element.find('[data-validate="false"]').each(function(){b.clearErrors(a(this))})),this.toggleSubmit(),this},d.prototype.onInput=function(b){var c=this,d=a(b.target),e="focusout"!==b.type;this.$inputs.is(d)&&this.validateInput(d,e).done(function(){c.toggleSubmit()})},d.prototype.validateInput=function(c,d){var e=(b(c),c.data("bs.validator.errors"));c.is('[type="radio"]')&&(c=this.$element.find('input[name="'+c.attr("name")+'"]'));var f=a.Event("validate.bs.validator",{relatedTarget:c[0]});if(this.$element.trigger(f),!f.isDefaultPrevented()){var g=this;return this.runValidators(c).done(function(b){c.data("bs.validator.errors",b),b.length?d?g.defer(c,g.showErrors):g.showErrors(c):g.clearErrors(c),e&&b.toString()===e.toString()||(f=b.length?a.Event("invalid.bs.validator",{relatedTarget:c[0],detail:b}):a.Event("valid.bs.validator",{relatedTarget:c[0],detail:e}),g.$element.trigger(f)),g.toggleSubmit(),g.$element.trigger(a.Event("validated.bs.validator",{relatedTarget:c[0]}))})}},d.prototype.runValidators=function(c){function d(a){return c.attr("data-"+a+"-error")}function e(){var a=c[0].validity;return a.typeMismatch?c.attr("data-type-error"):a.patternMismatch?c.attr("data-pattern-error"):a.stepMismatch?c.attr("data-step-error"):a.rangeOverflow?c.attr("data-max-error"):a.rangeUnderflow?c.attr("data-min-error"):a.valueMissing?c.attr("data-required-error"):null}function f(){return c.attr("data-error")}function g(a){return d(a)||e()||f()}var h=[],i=a.Deferred();return c.data("bs.validator.deferred")&&c.data("bs.validator.deferred").reject(),c.data("bs.validator.deferred",i),a.each(this.validators,a.proxy(function(a,d){var e=null;!b(c)&&!c.attr("required")||void 0===c.attr("data-"+a)&&"native"!=a||!(e=d.call(this,c))||(e=g(a)||e,!~h.indexOf(e)&&h.push(e))},this)),!h.length&&b(c)&&c.attr("data-remote")?this.defer(c,function(){var d={};d[c.attr("name")]=b(c),a.get(c.attr("data-remote"),d).fail(function(a,b,c){h.push(g("remote")||c)}).always(function(){i.resolve(h)})}):i.resolve(h),i.promise()},d.prototype.validate=function(){var b=this;return a.when(this.$inputs.map(function(){return b.validateInput(a(this),!1)})).then(function(){b.toggleSubmit(),b.focusError()}),this},d.prototype.focusError=function(){if(this.options.focus){var b=this.$element.find(".has-error:first :input");0!==b.length&&(a("html, body").animate({scrollTop:b.offset().top-d.FOCUS_OFFSET},250),b.focus())}},d.prototype.showErrors=function(b){var c=this.options.html?"html":"text",d=b.data("bs.validator.errors"),e=b.closest(".form-group"),f=e.find(".help-block.with-errors"),g=e.find(".form-control-feedback");d.length&&(d=a("<ul/>").addClass("list-unstyled").append(a.map(d,function(b){return a("<li/>")[c](b)})),void 0===f.data("bs.validator.originalContent")&&f.data("bs.validator.originalContent",f.html()),f.empty().append(d),e.addClass("has-error has-danger"),e.hasClass("has-feedback")&&g.removeClass(this.options.feedback.success)&&g.addClass(this.options.feedback.error)&&e.removeClass("has-success"))},d.prototype.clearErrors=function(a){var c=a.closest(".form-group"),d=c.find(".help-block.with-errors"),e=c.find(".form-control-feedback");d.html(d.data("bs.validator.originalContent")),c.removeClass("has-error has-danger has-success"),c.hasClass("has-feedback")&&e.removeClass(this.options.feedback.error)&&e.removeClass(this.options.feedback.success)&&b(a)&&e.addClass(this.options.feedback.success)&&c.addClass("has-success")},d.prototype.hasErrors=function(){function b(){return!!(a(this).data("bs.validator.errors")||[]).length}return!!this.$inputs.filter(b).length},d.prototype.isIncomplete=function(){function c(){var c=b(a(this));return!("string"==typeof c?a.trim(c):c)}return!!this.$inputs.filter("[required]").filter(c).length},d.prototype.onSubmit=function(a){this.validate(),(this.isIncomplete()||this.hasErrors())&&a.preventDefault()},d.prototype.toggleSubmit=function(){this.options.disable&&this.$btn.toggleClass("disabled",this.isIncomplete()||this.hasErrors())},d.prototype.defer=function(b,c){return c=a.proxy(c,this,b),this.options.delay?(window.clearTimeout(b.data("bs.validator.timeout")),void b.data("bs.validator.timeout",window.setTimeout(c,this.options.delay))):c()},d.prototype.reset=function(){return this.$element.find(".form-control-feedback").removeClass(this.options.feedback.error).removeClass(this.options.feedback.success),this.$inputs.removeData(["bs.validator.errors","bs.validator.deferred"]).each(function(){var b=a(this),c=b.data("bs.validator.timeout");window.clearTimeout(c)&&b.removeData("bs.validator.timeout")}),this.$element.find(".help-block.with-errors").each(function(){var b=a(this),c=b.data("bs.validator.originalContent");b.removeData("bs.validator.originalContent").html(c)}),this.$btn.removeClass("disabled"),this.$element.find(".has-error, .has-danger, .has-success").removeClass("has-error has-danger has-success"),this},d.prototype.destroy=function(){return this.reset(),this.$element.removeAttr("novalidate").removeData("bs.validator").off(".bs.validator"),this.$inputs.off(".bs.validator"),this.options=null,this.validators=null,this.$element=null,this.$btn=null,this.$inputs=null,this};var e=a.fn.validator;a.fn.validator=c,a.fn.validator.Constructor=d,a.fn.validator.noConflict=function(){return a.fn.validator=e,this},a(window).on("load",function(){a('form[data-toggle="validator"]').each(function(){var b=a(this);c.call(b,b.data())})})}(jQuery);


window.onload = function() {
  var totalPrice = document.getElementById('totalprice')
  var deposit = document.getElementById('deposit');
  totalPrice.addEventListener('keyup', calculateAmountToPay);
  deposit.addEventListener('keyup', calculateAmountToPay);
  function calculateAmountToPay () {
    var val_1 = document.getElementById('totalprice').value;
    var val_2 = document.getElementById('deposit').value;
    var amounttopay = document.getElementById('amounttopay');
    amounttopay.value = val_1 - val_2;
  }
  function checkHours () {

  }
};

window.setTimeout(function() {
  $("#success-alert").fadeTo(500, 0).slideUp(500, function(){
    $(this).remove();
  });
}, 3000);


ko.bindingHandlers.dateTimePicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().dateTimePickerOptions || {};
        $(element).datetimepicker(options);

        //when a user changes the date, update the view model
        ko.utils.registerEventHandler(element, "dp.change", function (event) {
            var value = valueAccessor();
            if (ko.isObservable(value)) {
                if (event.date != null && !(event.date instanceof Date)) {
                    value(event.date.toDate());
                } else {
                    value(event.date);
                }
            }
        });

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            var picker = $(element).data("DateTimePicker");
            if (picker) {
                picker.destroy();
            }
        });
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

        var picker = $(element).data("DateTimePicker");
        //when the view model is updated, update the widget
        if (picker) {
            var koDate = ko.utils.unwrapObservable(valueAccessor());

            //in case return from server datetime i am get in this form for example /Date(93989393)/ then fomat this
            koDate = (typeof (koDate) !== 'object') ? new Date(parseFloat(koDate.replace(/[^0-9]/g, ''))) : koDate;

            picker.date(koDate);
        }
    }
};

$(document).ready(function(){
  $('.slick-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slick-slider-nav'
  });
  $('.slick-slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slick-slider',
    dots: true,
    centerMode: true,
    focusOnSelect: true
  });
});
