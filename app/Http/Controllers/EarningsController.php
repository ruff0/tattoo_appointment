<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use Illuminate\Support\Facades\Auth;
use App\User;
use Carbon\Carbon;

class EarningsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $appointments = \App\Appointment::orderBy('appointment_date', 'DESC')->paginate(10);
      // return view('customers/index', ['page'=> 'Customers', 'customers' => $customers]);
      return view('earnings.index')->with(['page' => 'Earnings', 'appointments' => $appointments]);
    }
}
