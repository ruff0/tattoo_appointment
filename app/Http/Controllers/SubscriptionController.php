<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Stripe\Stripe;
use View;

class SubscriptionController extends Controller
{
  public function index() {
    return view('subscription.index');
  }
  public function settingspage() {
    $user = Auth::user();
    $plan_id = $user->plan_id;
    if ($plan_id == "tattoobookingapp_plan_1") {
      $plan = "Monthly Payment";
    }
    return view('subscription.settings')->with(['page' => 'Settings', 'user' => $user, 'plan' => $plan]);
  }

  public function getCancel()
  {
    Stripe::setApiKey(getenv('STRIPE_API_KEY'));
    $user = Auth::user();
    $user->subscription('main')->cancel();
    return redirect('settings')->with(['page' => 'Settings', 'success' => 'Subscription Canceled']);
  }

  public function getResume()
  {
    Stripe::setApiKey(getenv('STRIPE_API_KEY'));
    $user = Auth::user();
    $user->subscription('main')->resume();
    return redirect('settings')->with(['page' => 'Settings', 'success' => 'Subscription Resumed']);
  }

    public function getCard()
    {
      return View::make('subscription.card')->with(['page' => 'Settings']);
    }


    public function postCard(Request $request)
    {
      Stripe::setApiKey(getenv('STRIPE_API_KEY'));
      $user = Auth::user();
      $user->updateCard($request->input('stripeToken'));

      return redirect('settings')->with(['page' => 'Settings', 'success' => 'Card Updated']);
    }
}
