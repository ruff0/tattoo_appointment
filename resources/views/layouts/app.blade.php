<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <!-- Animation library for notifications   -->
    {{-- <link href="assets/css/animate.min.css" rel="stylesheet"/> --}}
    <!--  Light Bootstrap Table core CSS    -->
    {{-- <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/> --}}
    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    {{-- <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" /> --}}
    <!-- Scripts -->
    <!-- CSRF Token -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'InkCalendar') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        {{-- @include('inc.nav') --}}
        {{-- <div class="container"> --}}
          {{-- @include('inc.messages') --}}
          @yield('content')
        {{-- </div> --}}
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    {{-- <script src="https://malsup.github.com/jquery.form.js"></script> --}}
    <script src="{{ asset('js/jquery.form.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    {{-- <script type="text/javascript">
      $(document).on('click', '.pagination a',function(e){
        // e.stopImmediatePropagation();
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        getAppointments(page);
      });
      function getAppointments(page) {
        console.log('getting appointments for page =' + page);
        $.ajax({
          url: 'ajax/appointments?page='+page
        }).done(function(data){
          console.log(data);
          $('#upcomingAppointments').html(data);
        });
      }
    </script> --}}
    <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
