<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('customers', function (Blueprint $table) {
          $table->increments('id');
          $table->string('f_name');
          $table->string('l_name');
          $table->date('dob');
          $table->string('address_line_1');
          $table->string('address_line_2')->nullable();
          $table->string('city');
          $table->string('country');
          $table->string('email')->unique();
          $table->string('image_url')->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('customers');
    }
}
