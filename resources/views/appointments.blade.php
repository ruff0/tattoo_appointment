@foreach ($appointments as $appointment)
  <a href="appointments/{{$appointment->id}}">
    <div class="panel panel-default slideInUp">
      <div class="panel-body">
        <h5 class="pull-left">{{$appointment->title}} for {{$appointment->client_name}}</h5>
        <span class="pull-right">{{$appointment->appointment_date}} @ {{substr($appointment->start_time, 0, -3)}}</span>
      </div>
    </div>
  </a>
@endforeach
{{ $appointments->links() }}
