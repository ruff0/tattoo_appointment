{{-- {{$customer}} --}}
@extends('layouts.app')
  @section('content')
  <div class="wrapper">
    @include('inc.backsidebar')
    <div class="main-panel">
      @include('inc.nav')
      @include('inc.messages')
      <div class="content">
        <div class="card">
          {!! Form::open(['action' => ['CustomerController@update', $customer->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class'=>'create-form', 'data-toggle'=>'validator']) !!}
            <div class="form-group"> 
              {{Form::label('f_name', 'First Name')}}
              {{Form::text('f_name', $customer->f_name, ['class'=>'form-control', 'placeholder'=>'First Name', 'required'])}}
            </div>
            <div class="form-group">
              {{Form::label('l_name', 'Last Name')}}
              {{Form::text('l_name', $customer->l_name, ['class'=>'form-control', 'placeholder'=>'Last Name', 'required'])}}
            </div>
            <div class="form-group">
              {{Form::label('dob', 'Date of Birth')}}
              {{-- {{Form::text('date', '', ['id'=>'datePicker'])}} --}}
              {{Form::text('dob', $customer->dob, ['class'=>'form-control', 'id'=>'datePicker'])}}
            </div>
            <div class="form-group">
              {{Form::label('address_line_1', 'Address')}}
              {{Form::text('address_line_1', $customer->address_line_1, ['class'=>'form-control', 'placeholder'=>'Address', 'required'])}}
            </div>
            <div class="form-group">
              {{Form::label('address_line_2', 'Address')}}
              {{Form::text('address_line_2', $customer->address_line_2, ['class'=>'form-control', 'placeholder'=>'Address'])}}
            </div>
            <div class="form-group">
              {{Form::label('city', 'City')}}
              {{Form::text('city', $customer->city, ['class'=>'form-control', 'placeholder'=>'City', 'required'])}}
            </div>
            <div class="form-group">
              {{Form::label('country', 'Country')}}
              {{Form::text('country', $customer->country, ['class'=>'form-control', 'placeholder'=>'Country', 'required'])}}
            </div>
            <div class="form-group">
              {{Form::label('email', 'Email')}}
              {{Form::email('email', $customer->email, ['class'=>'form-control', 'placeholder'=>'Email@email.com', 'required'])}}
            </div>
            <div class="form-group">
              {{Form::label('phone_no', 'Phone')}}
              {{Form::number('phone_no', $customer->phone_no, ['class'=>'form-control', 'placeholder'=>'0871231234', 'required'])}}
            </div>
            {{Form::hidden('_method', 'PUT')}}
            {{Form::submit('Update', ['class'=>'btn btn-primary pull-right', 'id'=>'sbmtbtn'])}}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(function () {
      $('#datePicker').datetimepicker({
        inline: true,
        sideBySide: true,
        format : 'YYYY/MM/DD'
      });

    });
  </script>
@endsection
