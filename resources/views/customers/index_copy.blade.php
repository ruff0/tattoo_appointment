@extends('layouts.app')
@section('content')
<div class="wrapper">
  @include('inc.sidebar')
  <div class="main-panel">
    @include('inc.nav')
    @include('inc.messages')
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                      <div class="row">
                        {{-- 'data-toggle'=>'validator' --}}
                        {!! Form::open(['action' => 'CustomerController@index', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                          <div class="col-sm-4 col-md-2">
                            <div class="form-group">
                              {{-- Title --}}
                              {{Form::label('name', 'Name')}}
                              {{Form::text('name', '', ['class'=>'form-control', 'placeholder'=>'Name'])}}
                              {{-- <div class="help-block with-errors"></div> --}}
                            </div>
                          </div>
                          <div class="col-sm-4 col-md-2">
                            <div class="form-group">
                              {{-- Client Info --}}
                              {{Form::label('email', 'Email')}}
                              {{Form::email('email', '', ['class'=>'form-control', 'placeholder'=>'Email'])}}
                              {{-- <div class="help-block with-errors"></div> --}}
                            </div>
                          </div>
                          <div class="col-sm-4 col-md-2">
                            <div class="form-group">
                              {{Form::submit('Search', ['class'=>'btn btn-primary', 'id'=>'sbmtbtn'])}}
                            </div>
                          </div>
                        {!! Form::close() !!}
                        <button class="pull-right btn btn-primary" type="button" name="button">New Customer</button>
                      </div>
                        {{-- <div class="form-group">
                          {{Form::label('file', 'Upload Multiple Reference Meterial')}}
                        </div> --}}
                        {{-- <p class="category">Here is a subtitle for this table</p> --}}
                      </div>
                      <div class="content table-responsive table-full-width">
                          <table class="table table-hover table-striped">
                              <thead>
                                <th>View</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Age</th>
                                <th>ID</th>
                                <th>Edit</th>
                                <th>Delete</th>
                              </thead>
                              <tbody>
                                @foreach ($customers as $customer)
                                  <tr>
                                    <td><a href="customers/{{$customer->id}}">View</a></td>
                                    <td>{{$customer->f_name}}</td>
                                    <td>{{$customer->l_name}}</td>
                                    <td>{{$customer->address_line_1}}, {{$customer->address_line_2}}, {{$customer->city}}, {{$customer->country}}</td>
                                    <td>{{$customer->email}}</td>
                                    <td>{{$customer->dob}}</td>
                                    <td>{{$customer->image_url}}</td>
                                    <td><a href="#"><i class="pe-7s-pen"></i></a></td>
                                    <td><a href="#"><i class="pe-7s-delete-user"></i></a></td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                          <div class="footer text-center">
                            {{ $customers->links() }}
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
