<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>{{ config('app.name', 'Ttaatttooo') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('css/material-kit.css') }}" rel="stylesheet">
  <link href="{{ asset('css/material-landing.css') }}" rel="stylesheet">

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

</head>

<body class="index-page">
<!-- Navbar -->
<nav class="navbar navbar-transparent navbar-fixed-top navbar-color-on-scroll">
	<div class="container">
        <div class="navbar-header">
	    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index">
	        	<span class="sr-only">Toggle navigation</span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	    	</button>
	    	<a href="{{ url('/') }}">
	        	<div class="logo-container">
  	          {{-- <div class="logo">
                <img src="{{URL::to('/')}}/images/landing-img/logo.png" alt="Creative Tim Logo" rel="tooltip" title="<b>Material Kit</b> was Designed & Coded with care by the staff from <b>Creative Tim</b>" data-placement="bottom" data-html="true">
              </div> --}}
    	        <div class="brand">
    	           {{ config('app.name', 'Ttaatttooo') }}
    	        </div>
				    </div>
	      	</a>
	    </div>

	    <div class="collapse navbar-collapse" id="navigation-index">
	    	<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="{{ route('login') }}">
						<i class="material-icons">perm_identity</i> Login
					</a>
				</li>
				<li>
					<a href="{{ route('register') }}">
						<i class="material-icons">unarchive</i> Register
					</a>
				</li>
				{{-- <li>
					<a rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank" class="btn btn-white btn-simple btn-just-icon">
						<i class="fa fa-twitter"></i>
					</a>
				</li>
				<li>
					<a rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank" class="btn btn-white btn-simple btn-just-icon">
						<i class="fa fa-facebook-square"></i>
					</a>
				</li>
				<li>
					<a rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank" class="btn btn-white btn-simple btn-just-icon">
						<i class="fa fa-instagram"></i>
					</a>
				</li> --}}

	    	</ul>
	    </div>
	</div>
</nav>
<!-- End Navbar -->

<div class="wrapper">
	<div class="header header-filter" style="background-image: url('{{URL::to('/')}}/images/landing-img/header_bg.jpg');">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="brand">
						<h1>{{ config('app.name', 'Ttaatttooo') }}</h1>
						<h3>Simply Manager for Tattoo Appointments.</h3>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="main main-raised">
		<div class="container">
			<div class="section">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="title text-center">Let's talk product</h2>
						<p class="description">
							{{ config('app.name', 'Laravel') }} is a simple manager for your tattoo appointments.
							Was created specifically for tattoo artist.
							Many of them my friends, this program provides features such as:
						</p>
						<div class="row description">
							<div class="col-xs-4 col-sm-offset-2">
								<ul>
									<li>Manage Appointments</li>
									<li>Mark Appointments Cancled, Refunded or Complete</li>
									<li>Manage Customers</li>
								</ul>
							</div>
							<div class="col-xs-4 col-sm-offset-0 col-xs-offset-1">
								<ul>
									<li>Search Customers</li>
									<li>View / Create Earnings Reports</li>
									<li>Export Earnings as CSV</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-8 col-md-offset-2">

						<p class="description">


							Further features and improvements in TODO List:
						</p>
						<div class="row description">
							<div class="col-xs-4 col-sm-offset-2">
								<ul>
									<li>Language Selector</li>
									<li>Statistics</li>
									<!-- <li>SSL Certificate</li> -->
									<li>Visual Enhacement</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="section">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 text-center">
						<h2 class="title text-center">Plans</h2>
						<div class="row">
							<p class="text-center">There are two payment plans both of which currently
								 offer the same features. You can choose your own rate. Pay per month or
							 	 pay up front for a year. If you still sceptic then you can request a trial month by contacting us.</p>
							<div class="col-sm-4 col-sm-offset-2">
								<p class="text-center">1. $5 USD p/month</p>
							</div>
							<div class="col-sm-4">
								<p class="text-center">2. $50 USD p/year</p>
							</div>
						</div>
						<a href="{{ route('register') }}" class="btn btn-primary">Register Now</a>
					</div>
				</div>
			</div>
		</div>
	</div>
    <footer class="footer">
	    <div class="container">
	        <nav class="pull-left">
	            <ul>
					<li>
						<a href="http://400tres.com/cgi-bin/index2.cgi?main=contact2.rfk">
							contacto@400tres.com
						</a>
					</li>
					<li>
						<a href="{{ route('login') }}">
						   Login
						</a>
					</li>
					<li>
						<a href="{{ route('register') }}">
						   Register
						</a>
					</li>
					<li>
						<a href="http://facebook.com/400tres">
							Facebook
						</a>
					</li>
	            </ul>
	        </nav>
	        <div class="copyright pull-right">
	            &copy; 2018  Kliaki.com & 400tres.com
	        </div>
	    </div>
	</footer>
</div>

</body>
	<!--   Core JS Files   -->
	<script src="{{URL::to('/')}}/js/landing-js/jquery.min.js" type="text/javascript"></script>
	<script src="{{URL::to('/')}}/js/landing-js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{URL::to('/')}}/js/landing-js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="{{URL::to('/')}}/js/landing-js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="{{URL::to('/')}}/js/landing-js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="{{URL::to('/')}}/js/landing-js/material-kit.js" type="text/javascript"></script>

	<script type="text/javascript">

		$().ready(function(){
			// the body of this function is in assets/material-kit.js
			materialKit.initSliders();
            window_width = $(window).width();

            if (window_width >= 992){
                big_image = $('.wrapper > .header');

				$(window).on('scroll', materialKitDemo.checkScrollForParallax);
			}

		});
	</script>
</html>
{{-- @endsection --}}
