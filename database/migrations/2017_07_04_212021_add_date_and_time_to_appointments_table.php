<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateAndTimeToAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('appointments', function (Blueprint $table) {
          $table->date('appointment_date');
          $table->time('start_time');
          $table->time('end_time');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('appointments', function (Blueprint $table) {
          $table->dropColumn('appointment_date');
          $table->dropColumn('start_time');
          $table->dropColumn('end_time');
      });
    }
}
