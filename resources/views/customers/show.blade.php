{{-- {{$customer}} --}}
@extends('layouts.app')
@section('content')
<div class="wrapper">
  @include('inc.backsidebar')
  <div class="main-panel">
    @include('inc.nav')
    @include('inc.messages')
  <div class="content">
    <div class="card">
      @foreach ($customer as $cust)
        <div class="header">
          <h1>Customers</h1>
        </div>
        <div class="content">
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="header">
                  <h2>{{$cust->f_name}}'s Details</h2>
                </div>
                <div class="content">
                  <p><em>Name: </em>{{$cust->f_name}} {{$cust->l_name}}</p>
                  <p><em>Age: </em>{{$cust->age}}</p>
                  <p><em>Date of Birth: </em>{{$cust->dob}}</p>
                  <p><em>Address: </em>{{$cust->address_line_1}}, {{$cust->address_line_2}}, {{$cust->city}}, {{$cust->country}}</p>
                  <p><em>Email: </em>{{$cust->email}}</p>
                  <p><em>Phone: </em>{{$cust->phone_no}}</p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                <div class="header">
                  <h2>Upcoming Appointments</h2>
                </div>
                <div class="content">
                    {{-- <h4>{{$appointment->title}} for {{$appointment->customer->f_name}}</h4> --}}
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                              <th>View</th>
                              <th>Title</th>
                              {{-- <th>Description</th> --}}
                              <th>Date</th>
                              <th>Start Time</th>
                              <th>End Time</th>
                              <th>Edit</th>
                              <th>Delete</th>
                            </thead>
                            <tbody>
                              @foreach ($appointments as $appointment)
                                <tr>
                                  <td><a href="{{ url('/') }}/appointments/{{$appointment->id}}">View</a></td>
                                  <td>{{$appointment->title}}</td>
                                  {{-- <td>{{$appointment->description}}</td> --}}
                                  <td>{{$appointment->appointment_date}}</td>
                                  <td>{{$appointment->start_time}}</td>
                                  <td>{{$appointment->end_time}}</td>
                                  <td><a href="appointments/{{$appointment->id}}/edit"><i class="pe-7s-pen"></i></a></td>
                                  <td><a href="appointments/{{$appointment->id}}"><i class="pe-7s-delete-user"></i></a></td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                        <div class="footer text-center">
                          {{-- {{ $customers->links() }} --}}
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>
</div>
@endsection
