<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Appointment;
use App\User;
use View;
use Auth;

class SearchController extends Controller
{
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
      return view('search');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
      $loggedInUser = Auth::user();
      $user_id = $loggedInUser->id;
      //select("f_name")->
      $matches = Customer::where('user_id', $user_id)->where("f_name","LIKE","%{$request->input('q')}%")->get();
      return response()->json(['items' => $matches]);
    }
    public function customerSearch(Request $request)
    {
      $loggedInUser = Auth::user();
      $user_id = $loggedInUser->id;
      //select("f_name")->
      // $matches = Customer::where("f_name","LIKE","%{$request->input('q')}%")->get();
      // return response()->json(['items' => $matches]);
      $name = $request->input('name');
      $email = $request->input('email');
      $matches;
      if ((isset($name)) && (!isset($email))) {
        $matches = Customer::where("user_id",$user_id)->where("f_name","LIKE","%{$request->input('name')}%")->paginate(10);
      }
      if ((isset($email)) && (!isset($name))) {
        $matches = Customer::where("user_id",$user_id)->where("email","LIKE","%{$request->input('email')}%")->paginate(10);
      }
      if ((isset($email)) && (isset($name))) {
        $matches = Customer::where("user_id",$user_id)->where([["f_name","LIKE","%{$request->input('name')}%"],["email","LIKE","%{$request->input('email')}%"],])->paginate(10);
      }
      foreach ($matches as $customer) {
        $customer->dob = date_diff(date_create($customer->dob), date_create('today'))->y;
      }

      // return response()->json(['matches' => $matches]);
      return View::make('customers', ['page'=> 'Customers', 'customers' => $matches])->render();
    }


    public function earningsSearch(Request $request)
    {
      $loggedInUser = Auth::user();
      $user_id = $loggedInUser->id;
      $startDate = $request->input('startDate');
      $endDate = $request->input('endDate');
      $matches;

      // $matches = Appointment::where([["appointment_date","LIKE","%{$request->input('name')}%"],["email","LIKE","%{$request->input('email')}%"],])->paginate(10);
      $matches = Appointment::where("user_id",$user_id)->whereBetween('appointment_date', array($startDate, $endDate))->orderBy('appointment_date', 'desc')->paginate(10);

      // return response()->json(['matches' => $matches]);
      return View::make('earnings', ['page'=> 'Earnings', 'appointments' => $matches])->render();
    }
}
