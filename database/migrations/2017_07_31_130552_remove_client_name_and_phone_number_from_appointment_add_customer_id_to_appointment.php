<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveClientNameAndPhoneNumberFromAppointmentAddCustomerIdToAppointment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('appointments', function (Blueprint $table) {
        $table->dropColumn('client_name');
        $table->dropColumn('client_phone');
        $table->integer('client_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      $table->string('client_name');
      $table->string('client_phone');
      $table->dropColumn('client_id');
    }
}
