<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $beg = date('Y-m-01');
      $end = date("Y-m-t");
      return view('dashboard')->with('page', 'Dashboard');
    }

    public function getEarningsForWeek()
    {
      //getTodaysDAte
      $todaysDate = date("Y-m-d");

      //findHowManyDaysSinceMonday
      $currentDayNumber = date('w');
      $currentDayNumber = $currentDayNumber - 1;

      //getDateForMonday
      $weekStart = date('Y-m-d', strtotime('-'.$currentDayNumber.' days'));
      $weekEnd = strtotime($weekStart);
      $weekEnd = strtotime("+6 days", $weekEnd);
      $weekEnd = date('Y-m-d', $weekEnd);

      //Call Get Earnings
      $weeksEarnings = array();
      for ($i = 0; $i < 7 ; $i++) {
        $totalForDay = 0;
        $earnings = self::getEarnings($weekStart, $weekStart);

        foreach ($earnings as $earning) {
          // $appointmentObj = json_decode($earning);
          $totalForDay = $totalForDay + $earning->income;
        }

        $weekStart = strtotime($weekStart);
        $weekStart = strtotime("+1 days", $weekStart);
        $weekStart = date('Y-m-d', $weekStart);
        $weeksEarnings[] = $totalForDay;
      }

      return json_encode($weeksEarnings);
    }



    /**
     * Get Earnings.
     *
     * @return \Illuminate\Http\Response
     */
     public function getEarningsForMonth()
     {
       //Get start of month
       $beg = date('Y-m-01');
       //Get end of Month
       $end = date("Y-m-t");

       //Call Get Earnings
       $earnings = self::getEarnings($beg, $end);
       return $earnings;
     }



     /**
      * Get Earnings.
      *
      * @return \Illuminate\Http\Response
      */
      public function getEarningsForYear()
      {

        $monthEarnings = array();

        for ($i=1; $i <= 12 ; $i++) {
          //1. Get start date and end date of month
          //Get start of month
          $beg = date("Y-" . $i . "-01");

          //Get end of Month
          $end = date("Y-" . $i . "-t");

          //2. Get earnings for that month
          $earnings = self::getEarnings($beg, $end);

          //3. Add up all earnings for that month
          $totalForMonth = 0;
          $appointmentObj;
          foreach ($earnings as $earning) {
            // $appointmentObj = json_decode($earning);
            $totalForMonth = $totalForMonth + $earning->income;
          }

          //4. Add the earnings for that month to the ArrayAccess
          //4.1 Check if earnings for month = 0. If so, add 0 for that month
          $monthEarnings[] = $totalForMonth;
        }

        //Add up Earnings for Every month
        return json_encode($monthEarnings);
      }


    /**
     * Get Earnings.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEarnings($startDate, $endDate)
    {
      $loggedInUser = Auth::user()->id;
      $earnings = DB::table('appointments')
        ->select('appointment_date', 'total_cost','deposit_paid', 'complete', 'canceled', 'refunded')
        ->where('user_id', $loggedInUser)
        ->whereBetween('appointment_date', [$startDate, $endDate])
        ->orderBy('appointment_date', 'asc')
        ->get();

      foreach ($earnings as $earning) {
        if ($earning->complete) {
          $earning->income = $earning->total_cost;
        }
        else if ($earning->canceled) {
          $earning->income = $earning->deposit_paid;
        }
        else if ($earning->refunded) {
          $earning->income = 0;
        }
        else {
          $earning->income = $earning->total_cost;
        }
      }
      return $earnings;
    }
}
