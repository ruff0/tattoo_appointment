<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalcostDepositAndAmounttopayToAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('appointments', function (Blueprint $table) {
          $table->string('total_cost');
          $table->string('deposit_paid');
          $table->string('amount_to_pay');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('appointments', function (Blueprint $table) {
          $table->dropColumn('total_cost');
          $table->dropColumn('deposit_paid');
          $table->dropColumn('amount_to_pay');
      });
    }
}
