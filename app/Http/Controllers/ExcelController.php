<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use App\Customer;
use Input;
use DB;
use Auth;
Use Excel;

class ExcelController extends Controller
{
  public function getExport(Request $request) {
    $loggedInUser = Auth::user();
    $user_id = $loggedInUser->id;

    $startDate = $request->input('startDate');
    $endDate = $request->input('endDate');
    // $appointments = Appointment::select('total_cost as Total(€)', 'title as Title', 'appointment_date as Date', 'client_id')
    $appointments = Appointment::where('user_id', $user_id)
      ->whereBetween('appointment_date', array($startDate, $endDate))
      ->orderBy('appointment_date', 'desc')
      ->get();

    $customer;
    foreach ($appointments as $appointment) {
      $customer = Customer::find($appointment->client_id);
      $appointment->Client_Name = $customer->f_name . " " . $customer->l_name;
    }

    // Excel::create('Export Data', function($excel) use($appointments){
    //   $excel->sheet('Sheet 1', function($sheet) use($appointments){
    //     $sheet->fromArray($appointments);
    //   });
    // })->export('csv');

    Excel::create('Export Data', function($excel) use($appointments){
      $excel->sheet('Sheet 1', function($sheet) use($appointments){
        $sheet->loadView('excel.export')->with('appointments', $appointments);
        // $sheet->loadView('excel.export');
      });
    })->export('csv');
  }
}
