<?php

namespace App\Listeners;

use App\Events\AppointmentCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateCalendarEvent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AppointmentCreated  $event
     * @return void
     */
    public function handle(AppointmentCreated $event)
    {
      $Appointment = $event->user;

      return redirect('/')->with('success', 'Appointment Passed To Listener');

      //1. Take Appointment Information and Create an Event.

      //2. Add event to event table? Maybe?

      //3. Return redirect to index.

    }
}
