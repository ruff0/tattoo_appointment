@extends('layouts.app')
@section('content')
  <div class="wrapper">
    @include('inc.sidebar')
      <div class="main-panel">
          @include('inc.nav')
          @include('inc.messages')
          <div class="content">
            {{-- {{$subscribed}} --}}
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-md-6">
                          <div class="card">
                              <div class="header">
                                  <h4 class="title">Upcoming Appointments</h4>
                                  <p class="category"></p>
                              </div>
                              <div class="content">
                                <div id="upcomingAppointments" class="">
                                  @include('appointments')
                                </div>
                                <div class="footer">
                                </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                        <div class="card">
                          <div class="header">
                            <h4 class="title">Earnings So Far</h4>
                            <p class="category">Your Earnings to Date</p>
                          </div>
                          <div class="content">
                            <div id="chartPreferences" class="ct-chart ct-perfect-fourth"></div>
                            {{-- <hr> --}}
                            <div class="footer">
                              <div class="legend">
                                {{-- <i class="fa fa-circle text-info"></i> Open --}}
                              </div>
                              <div class="chart-controls pull-right">
                                <span class="btn btn-sm btn-primary" id="chart-week">Week</span>
                                {{-- <span class="btn btn-sm btn-primary" id="chart-month">Month</span> --}}
                                <span class="btn btn-sm btn-primary" id="chart-year">Year</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>


          <footer class="footer">
              <div class="container-fluid">
                  <nav class="pull-left">
                      <ul>
                          <!-- <li>
                              <a href="#">
                                  Home
                              </a>
                          </li>
                          <li>
                              <a href="#">
                                  Company
                              </a>
                          </li>
                          <li>
                              <a href="http://400tres.com/profile/#portfolio-heading">
                                  Portfolio
                              </a>
                          </li>
                          <li>
                              <a href="#">
                                 Blog
                              </a>
                          </li> -->
                      </ul>
                  </nav>
                  <p class="copyright pull-right">
                      &copy; 2018 <a href="http://400tres.com">400tres.com</a>
                  </p>
              </div>
          </footer>
          <script type="text/javascript">
            $(document).on('click', '.pagination a',function(e){
              // e.stopImmediatePropagation();
              e.preventDefault();
              var page = $(this).attr('href').split('page=')[1];
              getAppointments(page);
            });
            function getAppointments(page) {
              console.log('getting appointments for page =' + page);
              $.ajax({
                url: 'ajax/appointments?page='+page
              }).done(function(data){
                console.log(data);
                $('#upcomingAppointments').html(data);
              });
            }
          </script>

      </div>
  </div>
@endsection
